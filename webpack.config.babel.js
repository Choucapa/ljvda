/*
 * Common Webpack Config
 */

import webpack from 'webpack';
import path from 'path';
import fs from 'fs';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import packageJSON from './package.json';

const ExtractPDFStyles = new MiniCssExtractPlugin({
    filename: '../app/static/pdf/pdf.css'
});

const nodeModules = {};
fs.readdirSync('node_modules')
    .filter(x => ['.bin'].indexOf(x) === -1)
    .forEach(mod => {
        nodeModules[mod] = `commonjs ${mod}`;
    });

module.exports = {
    entry: path.join(__dirname, 'app/App'),
    node: {
        __dirname: false,
        __filename: false
    },
    target: 'node',
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'App.js'
    },
    externals: nodeModules,
    devtool: 'source-map',
    plugins: [
        new webpack.BannerPlugin({
            banner: 'require("source-map-support").install();',
            raw: true,
            entryOnly: false,
            exclude: /pdf\.css/i
        }),
        new webpack.DefinePlugin({
            'process.env.VERSION': JSON.stringify(packageJSON.version)
        }),
        ExtractPDFStyles
    ],
    module: {
        rules: [
            {
                test: /\.js|jsx$/,
                use: ['babel-loader'],
                include: [path.join(__dirname, 'app'), path.join(__dirname, '../shared')],
                exclude: /node_modules/
            },
            {
                test: /\.(html|txt)$/,
                use: [
                    {
                        loader: 'ejs-loader',
                        options: {
                            esModule: false
                        }
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(json|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.-]+)?$/,
                use: ['file-loader']
            }
        ]
    },
    resolve: {
        modules: ['node_modules'],
        extensions: ['.html', '.txt', '.js', '.jsx', '.json', '.css', '.scss']
    }
};
