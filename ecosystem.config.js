const packageJSON = require('./package.json');

const sharedConfig = {
    script: 'dist/App.js',
    env: {
        NODE_ENV: 'development',
        HOME: './',
        VERSION: packageJSON.version
    },
    env_staging: {
        NODE_ENV: 'staging',
        VERSION: packageJSON.version
    },
    env_production: {
        NODE_ENV: 'production',
        VERSION: packageJSON.version
    }
};

module.exports = {
    apps: [
        {
            name: 'ljvda-api',
            cwd: '/app/dwwm/ljvda-api',
            ...sharedConfig
        }
    ]
};
