/*****************************************************************************
 * Data Configuration
 *****************************************************************************/

import TYPES from '../utils/PropsTypes';

// Collections Names
export const TABLES = {
    MEMBRES: 'membre',
    JEUX: 'jeux',
    COMMENTAIRES: 'commentaire',
};

/*
 * Primary Keys Names
 */
export const PRIMARY_KEYS = {
    [TABLES.JEUX]: 'jeu_id',
    [TABLES.MEMBRES]: 'membre_id',
    [TABLES.COMMENTAIRES]: 'commentaire_id',
};

export const PROPERTIES = [];

PROPERTIES[TABLES.MEMBRES] = {
    membre_id: [TYPES.number],
    pseudo: [TYPES.string],
    motdepasse: [TYPES.string],
    email: [TYPES.string],
    token: [TYPES.string],
    created_at: [TYPES.datetime],
};

PROPERTIES[TABLES.JEUX] = {
    jeu_id: [TYPES.number],
    nomJeu: [TYPES.string],
    support: [TYPES.strin],
    online: [TYPES.boolean],
    jaquette: [TYPES.string],
    created_at: [TYPES.datetime],
    description: [TYPES.string],

};

PROPERTIES[TABLES.COMMENTAIRES] = {
    commentaire_id: [TYPES.number],
    jeu_id: [TYPES.number],
    membre_id: [TYPES.number],
    com: [TYPES.string],
    note: [TYPES.string],
    created_at: [TYPES.datetime],
    played: [TYPES.boolean],
};

export const HAS_AUTOINCREMENT = {
    [TABLES.MEMBRES]: true,
    [TABLES.JEUX]: true,
    [TABLES.COMMENTAIRES]: true,
};
