/*****************************************************************************
 * Constants
 *****************************************************************************/

import { SERVER } from './ServerConfig';

export const APP_URL_STATIC = `${SERVER.APP_URL}/static`;
export const DATE_FORMAT = 'DD/MM/YYYY';
export const UPLOAD_DIR = `${__dirname}/../public/uploads`;
export const AUTHORIZED_UPLOAD_TYPES = [
    'image/gif',
    'image/jpeg',
    'image/png',
];

export const WKHTMLTOPDF_OPTIONS = {
    pageSize: 'A4',
    printMediaType: true,
    orientation: 'portrait',
    dpi: 300,
    encoding: 'UTF-8',
    disableSmartShrinking: true,
    T: 16,
    L: 10,
    R: 10,
    B: 20,
    footerHtml: `${APP_URL_STATIC}/pdf/footer.html`,
    headerHtml: `${APP_URL_STATIC}/pdf/header.html`
};
