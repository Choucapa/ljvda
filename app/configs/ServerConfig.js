/*****************************************************************************
 * Server Configuration
 *****************************************************************************/

import { existsSync } from 'fs';
import dotenv from 'dotenv';

dotenv.config(
    process.env.NODE_ENV === 'development' && !existsSync('./.env')
        ? { path: './.env.example' }
        : null
);

/*
 * API Server Configuration
 */
const SERVER = {
    APP_PORT: process.env.APP_PORT,
    APP_URL: process.env.APP_URL,
    APP_URL_FRONT: process.env.APP_URL_FRONT || process.env.APP_URL,
    SPARKPOST_API_KEY: process.env.SPARKPOST_API_KEY,
    LOCALHOST_TOKEN: process.env.LOCALHOST_TOKEN,
    MJ_APIKEY_PUBLIC: process.env.MJ_APIKEY_PUBLIC,
    MJ_APIKEY_PRIVATE: process.env.MJ_APIKEY_PRIVATE,
    IS_MAILJET: (process.env.IS_MAILJET && Number(process.env.IS_MAILJET)) || 0
};

/*
 * Database Configuration
 */
const DB = {
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_USER: process.env.DB_USER,
    DB_PASSWORD: process.env.DB_PASSWORD,
    DB_NAME: process.env.DB_NAME,
    DB_EXTRA: process.env.DB_EXTRA || {}
};

/*
 * MAIL Configuration
 */
const MAIL = {
    FROM: process.env.FROM,
    SUBJECT_PREFIX: process.env.SUBJECT_PREFIX,
    CONTACT_EMAIL: process.env.CONTACT_EMAIL
};

/*
 * Other Configuration
 */
const PDFTK_PATH = process.env.PDFTK_PATH || 'pdftk';

export { SERVER, DB, MAIL, PDFTK_PATH };
