/*****************************************************************************
 * API Server
 *****************************************************************************/

/*
 * Import Vendors
 */
import cluster from 'cluster';
import express from 'express';
import {Server as httpServer} from 'http';
import compress from 'compression';
import logger from 'morgan';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import os from 'os';
import path from 'path';


// import socketIO from 'socket.io';
//import expressStatic from 'express-static';
/*
 * Import Configs
 */
import {SERVER} from './configs/ServerConfig';
import loggerFormat from './utils/loggerFormat';
import {checkHeadersAcceptJSONResponse} from './utils/RequestsUtils';


/*
 * Import Router
 */
import ApplicationRoutes from './routes/index';

if (cluster.isPrimary) {
    // Master cluster (start an API for each CPU core)
    console.log(`LesJVnodeJs Server v${process.env.VERSION}`);

    // Create a worker for each CPU
    const cpuCount = process.env.APP_CLUSTER || os.cpus().length;
    for (let i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    // Recreate worker when dead
    cluster.on('exit', worker => {
        console.error(`API Worker ${worker.id} died :(`);
        cluster.fork();
    });
} else {
    // API cluster (a CPU core)
    /*
     * App creation
     */

    const app = express();
    const requestIp = require('request-ip');

    const localpath = process.env.NODE_ENV === 'production' ? '../../../../current/' : '../../';
    const http = httpServer(app);
    // app.ioSocket = socketIO(http);

    // get IP
    app.use(requestIp.mw());

    app.use(compress());

    app.use(bodyParser.json({limit: '150mb'}));
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(fileUpload());
    app.use(logger(loggerFormat));
    app.use(express.static(path.join(__dirname, localpath + 'lesjvdanaellenodejs/static')));
    app.use('/static', express.static(path.join(__dirname, '../app/static')));
    if (process.env.NODE_ENV === 'development') {
        app.use(require('cors')());
    }

    //---------------------------NOTIF-----------------------------------------
    // NOTIF public/private keys

    const mailVapidKey = process.env.MAIL_VAPID_KEY;
    const publicVapidKey = process.env.PUBLIC_VAPID_KEY;
    const privateVapidKey = process.env.PRIVATE_VAPID_KEY;

    //webpush.setVapidDetails(`mailto:${mailVapidKey}`, publicVapidKey, privateVapidKey);

    /*
     * API routes
     */
    app.use(checkHeadersAcceptJSONResponse);
    app.use('/api', ApplicationRoutes);

    /*
     * Go, go, go :D
     */
    http.listen(SERVER.APP_PORT, () => {
        if (cluster.isWorker) {
            console.log(
                `API worker # ${cluster.worker.id} listening on port ${SERVER.APP_PORT} in ${process.env.NODE_ENV} environment.`
            );
        }
        console.log(
            `API single worker listening on port ${SERVER.APP_PORT} in ${process.env.NODE_ENV} environment.`
        );
    });
}
