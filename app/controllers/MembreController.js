import { get, add, update, remove } from '../utils/DAO';
import { TABLES } from '../configs/DataConfig';

import { validateData } from '../utils/validateData';
const Joi = require('joi');

export function getMembres() {
    return get({
        type: TABLES.MEMBRES
    });
}

export function createMembre(data) {
    return add({
        type: TABLES.MEMBRES,
        data
    }).then(rows => {
        return {
            ...data,
            jeu_id: rows.insertId
        };
    });
}

export function updateMembre(id, data) {
    return update({
        type: TABLES.MEMBRES,
        data,
        where: { jeu_id: id }
    }).then(rows => {
        if (rows.affectedRows !== 1) {
            throw String('Impossible de mettre à jour le jeu');
        }
        return { jeu_id: id, ...data };
    });
}

export function deleteMembre(id) {
    return remove({
        type: TABLES.MEMBRES,
        where: { jeu_id: id }
    }).then(rows => {
        if (rows.affectedRows !== 1) {
            throw String('Impossible de supprimer le jeu');
        }
    });
}
