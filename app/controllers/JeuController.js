import {add, get, remove, update} from '../utils/DAO';
import {TABLES} from '../configs/DataConfig';

const Joi = require('joi');

export function getJeux() {
    return get({
        type: TABLES.JEUX
    });
}

export function getJeuById(id) {
    return get({
        type: TABLES.JEUX,
        where: {_id: id}
    }).then((res) => console.log("test", res));
}

export function createJeu(data) {
    console.log("GOOD JOB",data)
    return add({
        type: TABLES.JEUX,
        data
    }).then(rows => {
        return {
            ...data,
            jeu_id: rows.insertId
        };
    });
}

export function updateJeu(id, data) {
    return update({
        type: TABLES.JEUX,
        data,
        where: {jeu_id: id}
    }).then(rows => {
        if (rows.affectedRows !== 1) {
            throw String('Impossible de mettre à jour le jeu');
        }
        return {jeu_id: id, ...data};
    });
}

export function deleteJeu(id) {
    return remove({
        type: TABLES.JEUX,
        where: {jeu_id: id}
    }).then(rows => {
        if (rows.affectedRows !== 1) {
            throw String('Impossible de supprimer le jeu');
        }
    });
}
