/*****************************************************************************
 * Account Controller
 * Manage Accounts
 *****************************************************************************/

import SHA from 'js-sha512';
import { omit, toLower } from 'lodash';
import moment from 'moment';
moment.locale('fr');
import { update, get, aggregate } from '../utils/DAO';
import { validateData } from '../utils/validateData';
import { generateHash } from '../utils/HashGenerator';
import { TABLES } from '../configs/DataConfig';
import { sendTemplateMail } from '../utils/Mailer';
import { TEMPLATES_MAIL } from '../templates/mails/index';

const Joi = require('joi');

/*
 * Do Login
 * Return: The Authorization Token
 */

export function loadUser(userId) {
    return get({
        type: TABLES.MEMBRES,
        select: { membre_id: true, pseudo: true, email: true, role: true },
        where: { membre_id: userId }
    })
    .then(rows => {
        if (rows.length === 0) {
            throw String("Impossible de récupérer l'user");
        }
        return rows[0];
    });
}

export function doLogin(data) {
    if (!data.password) {
        throw String('Mail inconnu ou mauvais mot de passe');
    }
    let loginData = {
        membre_id: '',
        authorization_token: '',
        reset_token: ''
    };
    let userData;
    return get({
        type: TABLES.MEMBRES,
        select: { membre_id: true, pseudo: true, email: true, role: true },
        where: { email: toLower(data.email), password:  SHA.sha512(data.password) }
    })
        .then(rows => {
            if (!rows || rows.length !== 1) {
                throw String('Mail inconnu ou mauvais mot de passe');
            }
            loginData.membre_id = rows[0].membre_id;

            userData = omit(rows[0], ['password']);
        })
        .then(res => {
            return generateHash(undefined, 'Cannot generate an token');
        })
        .then(authorizationToken => {
            loginData.token = authorizationToken;
            return {
                ...loginData,
                ...userData
            };
        });
}

/*
 * Do Login Admin
 * Return: The Authorization Token
 */
export function doLoginAdmin(data) {
    if (!data.password) {
        throw String('Mail inconnu ou mauvais mot de passe');
    }

    let loginData = {
        membre_id: '',
        authorization_token: '',
        reset_token: ''
    };

    let userData;

    return get({
        type: TABLES.MEMBRES,
        where: {
            email: toLower(data.email),
            password: SHA.sha512(data.password),
            role: { $in: ['ADMIN'] }
        }
    })
        .then(rows => {
            if (!rows || rows.length !== 1) {
                throw String('Mail inconnu ou mauvais mot de passe');
            }
            loginData.membre_id = rows[0].membre_id;
            userData = rows[0];
        })
        .then(res => {
            return generateHash(undefined, 'Cannot generate an Authorization token');
        })
        .then(authorizationToken => {
            loginData.token = authorizationToken;
            return {
                ...loginData,
                ...userData
            };
        });
}

/*
 * recoverPassword
 * Return: matching user with new password
 */
export function recoverPassword(data) {
    let usersData = {};

    return get({
        type: TABLES.MEMBRES,
        select: { membre_id: true, pseudo: true, email: true, role: true },
        where: { email: toLower(data.email) }
    })
        .then(rows => {
            if (rows.length === 0) {
                throw String('Email non trouvé');
            }
            usersData = rows[0];

            return generateHash(undefined, 'Cannot generate a Reset token');
        })
        .then(generatedHash => {
            usersData.token = generatedHash;

            return update({
                type: TABLES.MEMBRES,
                data: {
                    reset_token: SHA.sha512(usersData.token)
                },
                where: { membre_id: usersData.membre_id }
            });
        })
        .then(rows => {
            if (rows.affectedRows !== 1) {
                throw String('Unable to update the Reset token');
            }
            return sendTemplateMail(
                TEMPLATES_MAIL.FORGOTTEN_PASSWORD,
                { user: usersData, user_id: usersData.user_id },
                usersData.email,
                {},
                false
            ),usersData.role;

        });
}

export function resetPassword(data) {
    return get({
        type: TABLES.MEMBRES,
        where: {
            email: toLower(data.email),
            membre_id: data.membre_id
        }
    })
        .then(rows => {
            if (rows.length !== 1) {
                throw String('Impossible de récupérer le compte');
            }
            return update({
                type: TABLES.MEMBRES,
                data: {
                    password: SHA.sha512(data.password)
                },
                where: {
                    membre_id: rows[0].membre_id
                }
            });
        })
        .then(rows => {
            if (rows.affectedRows !== 1) {
                throw String('Impossible de mettre à jour le mot de passe');
            }
        });
}

export function checkEmailExist(data) {
    const schema = Joi.object({
        email: Joi.string()
            .email({ minDomainSegments: 2 })
            .required()
    });

    if (validateData(schema, data)) {
        return get({
            type: TABLES.MEMBRES,
            where: { email: toLower(data.email) }
        }).then(rows => {
            if (rows.length === 1) {
                throw 'Cette adresse e-mail est déja utilisée par un autre compte';
            }
        });
    }
}

export function getRole(authorization_token) {
    return get({
        type: TABLES.MEMBRES,
        where: {
            authorization_token: authorization_token
        },
        select: {
            role: 1,
            email: 1
        }
    }).then(rows => {
        if (rows.length === 0) {
            throw String('Impossible de récupérer le role');
        }
        return rows[0];
    });
}
