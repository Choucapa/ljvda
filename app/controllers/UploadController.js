/*****************************************************************************
 * Upload Controller
 * Manage Uploads
 *****************************************************************************/

import moment from 'moment';
import fs from 'fs';
import { map } from 'lodash';
import slug from 'slug';

import { UPLOAD_DIR, AUTHORIZED_UPLOAD_TYPES } from '../configs/Constants';
import { getExtension, removeExtension } from '../utils/StringUtils';

/*
 * Send Files
 * Return: The URLs of created Files
 */
export function sendFiles(files, fileCallback) {
    console.log('Send Files', files);

    if (!files) {
        throw String('No files to send');
    } else if (!Array.isArray(files)) {
        files = [files];
    }

    const acceptedFiles = files.filter(
        file =>
            file.name && file.data && file.mimetype && AUTHORIZED_UPLOAD_TYPES.indexOf(file.mimetype) >= 0
    );

    return Promise.all(
        map(acceptedFiles, file =>
            new Promise((resolve, reject) => {
                const fileName = `${moment().format('YYYYMMDD-HHmmss')}-${slug(
                    removeExtension(file.name),
                    slug.defaults.modes.rfc3986
                )}${getExtension(file.name)}`;

                file.mv(`${UPLOAD_DIR}/${fileName}`, err => {
                    if (err) {
                        console.error(err);
                        reject(String(`Unable to move the file: '${file.name}'`));
                    }
                    return resolve(fileName);
                });
            }).then(fileName =>
                typeof fileCallback === 'function' ? fileCallback({ file, fileName }) : fileName
            )
        )
    );
}

/*
 * Send Files and add row in DB
 * Return: The URLs of created Files
 */
export function sendFilesWithDB(files) {
    return sendFiles(files).then(res => {
        const fileName = res[0];

        return { fileName: fileName, fileType: files.mimetype };
    });
}

/*
 * Delete Files
 */
export function deleteFiles(fileNames, fileCallback) {
    if (!Array.isArray(fileNames)) {
        fileNames = [fileNames];
    }

    return Promise.all(
        map(fileNames, fileName =>
            new Promise((resolve, reject) => {
                fs.unlink(`${UPLOAD_DIR}/${fileName}`, err => {
                    if (err) {
                        console.error(err);
                        reject(String(`Impossible de supprimer le fichier: '${fileName}'`));
                    }
                    return resolve(fileName);
                });
            }).then(() => (typeof fileCallback === 'function' ? fileCallback(fileName) : fileName))
        )
    );
}
