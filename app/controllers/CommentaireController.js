import { get, add, update, remove } from '../utils/DAO';
import { TABLES } from '../configs/DataConfig';

import { validateData } from '../utils/validateData';
const Joi = require('joi');

export function getCommentaires() {
    return get({
        type: TABLES.COMMENTAIRES
    });
}

export function createCommentaire(data) {
    return add({
        type: TABLES.COMMENTAIRES,
        data
    }).then(rows => {
        return {
            ...data,
            commentaire_id: rows.insertId
        };
    });
}

export function updateCommentaire(id, data) {
    return update({
        type: TABLES.COMMENTAIRES,
        data,
        where: { commentaire_id: id }
    }).then(rows => {
        if (rows.affectedRows !== 1) {
            throw String('Impossible de mettre à jour le commentaire');
        }
        return { commentaire_id: id, ...data };
    });
}

export function deleteCommentaire(id) {
    return remove({
        type: TABLES.COMMENTAIRES,
        where: { commentaire_id: id }
    }).then(rows => {
        if (rows.affectedRows !== 1) {
            throw String('Impossible de supprimer le commentaire');
        }
    });
}
