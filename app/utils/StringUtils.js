export function upperFirst(string) {
    return string[0].toUpperCase() + string.substr(1);
}

export function pad(source, length, char = '0') {
    const string = source.toString();
    return string.length < length ? pad(char + string, length, char) : string;
}

export function getExtension(filename) {
    var i = filename.lastIndexOf('.');
    return i < 0 ? '' : filename.substr(i);
}

export function removeExtension(filename) {
    var lastDotPosition = filename.lastIndexOf('.');
    if (lastDotPosition === -1) return filename;
    else return filename.substr(0, lastDotPosition);
}
