/*****************************************************************************
 * Error Utils
 *****************************************************************************/

/*
 * API Server Errors Code
 */
export const ERROR_API = {
    INTERNAL: 'INTERNAL_ERROR',
    DATA_BAD_BODY: 'ER_DATA_BAD_BODY',
    REQUEST_BAD_HEADERS: 'ER_REQUEST_BAD_HEADERS',
    BAD_AUTHORIZATION: 'BAD_AUTHORIZATION',
    FORBIDDEN_ACCESS: 'FORBIDDEN_ACCESS',
    BAD_AUTHORIZATION_GOOGLE_API: 'BAD_AUTHORIZATION_GOOGLE_API',
};

/*
 * Database Errors Code
 */
export const ERROR_DB = {
    // CONNECTION: 'ER_ACCESS_DENIED_ERROR',
    DUPLICATE: 'ER_DUP_ENTRY',
};

/*
 * API Server Error Response
 */
export function sendErrorResponse (err = {}, res, msg = 'Error') {
    if (err.message) {
        console.error(err.message);
    }
    if (!err.hideStack && err.stack) {
        console.error(err.stack);
    }

    let errorMessage = '';
    if (err.statusText) {
        errorMessage = `${msg}: ${err.statusText}`;
    } else if (typeof err === 'string') {
        errorMessage = `${msg}: ${err}`;
    } else {
        errorMessage = msg;
    }
    console.error(errorMessage);

    return res.status(err.statusCode || 500).send({
        status: 'error',
        code: err.code || ERROR_API.INTERNAL,
        data: err.data,
        message: errorMessage,
    });
}
