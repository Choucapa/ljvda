/*****************************************************************************
 * Request Utils Controller
 *****************************************************************************/

import SHA from 'js-sha512';
import { find } from 'lodash';

import { get } from './DAO';
import { ERROR_API, sendErrorResponse } from './ErrorUtils';
import { SERVER } from '../configs/ServerConfig';
import { TABLES } from '../configs/DataConfig';
import { isAdmin, isUser, canDoSomething } from './RolesUtils';

/*
 * Check that Headers Accept JSON responses
 */
export function checkHeadersAcceptJSONResponse(req, res, next) {
    if (!req.accepts('application/json')) {
        return sendErrorResponse(
            {
                statusCode: 406,
                code: ERROR_API.REQUEST_BAD_HEADERS
            },
            res,
            "Headers error: Doesn't Accept JSON responses"
        );
    }

    res.contentType('application/json');
    return next();
}

/*
 * Check Authentication in headers
 */
export function checkAuthentication(req, res, next) {
    console.log('Check Authentication in Headers');

    const accountid = req.headers.accountid || req.query.accountid;
    const authorization = req.headers.authorization || req.query.authorization;

    return Promise.resolve()
        .then(() => {
            if (!accountid || !authorization || authorization.length === 0) {
                console.error(`Headers error: userid=${accountid} / token=${authorization}`);
                throw Object({
                    statusCode: 401,
                    code: ERROR_API.BAD_AUTHORIZATION,
                    statusText: 'Request without userid or authorization'
                });
            }
            return get({
                type: TABLES.USER_CONNECTIONS,
                where: { account_id: parseInt(accountid, 0) },
                orderBy: { created_at: -1 },
                limit: 5
            });
        })
        .then(rows => {
            const matchedRow = find(
                rows,
                row =>
                    row && row.token && row.token === SHA.sha512(authorization)
            );

            if (matchedRow) {
                console.log(`User connected with ID: ${accountid}`);
                return get({
                    type: TABLES.ACCOUNTS,
                    select: { account_id: true, roles: true },
                    where: { account_id: parseInt(accountid, 0) }
                });
            }
            throw Object({
                statusCode: 401,
                code: ERROR_API.BAD_AUTHORIZATION,
                statusText: 'Request with bad userid or authorization'
            });
        })
        .then(rows => {
            req.account = rows[0];
            return next();
        })
        .catch(err =>
            sendErrorResponse(err, res, 'An error occurred when checking the authorization token')
        );
}

/*
 * Check that user roles is ADMIN
 */
export function checkAdminRole(req, res, next) {
    if (!req.account || !isAdmin(req.account)) {
        return sendErrorResponse(
            {
                statusCode: 403,
                code: ERROR_API.FORBIDDEN_ACCESS,
                statusText: 'This route require an Administrator role.'
            },
            res,
            'An error occurred when checking the authorization role'
        );
    }

    return next();
}

/*
 * Check that user roles is USER
 */
export function checkUserRole(req, res, next) {
    if (!req.account || !isUser(req.account)) {
        return sendErrorResponse(
            {
                statusCode: 403,
                code: ERROR_API.FORBIDDEN_ACCESS,
                statusText: 'This route requires an User role.'
            },
            res,
            'An error occurred when checking the authorization role'
        );
    }

    return next();
}

/*
 * Check that user roles can Do Something [Example]
 */
export function checkDoSomethingtRole(req, res, next) {
    if (!req.account || !canDoSomething(req.account)) {
        return sendErrorResponse(
            {
                statusCode: 403,
                code: ERROR_API.FORBIDDEN_ACCESS,
                statusText: 'This route require a Do Something role.'
            },
            res,
            'An error occurred when checking the authorization role'
        );
    }

    return next();
}

/*
 * Check that request is send from Localhost (for Batches)
 */
export function checkLocalhostRole(req, res, next) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    if (
        (ip !== '::1' && ip !== '127.0.0.1' && ip !== '::ffff:127.0.0.1') ||
        req.headers.accountid !== req.headers.authorization ||
        req.headers.accountid !== SERVER.LOCALHOST_TOKEN
    ) {
        return sendErrorResponse(
            {
                statusCode: 403,
                code: ERROR_API.FORBIDDEN_ACCESS,
                statusText: 'Cette route requiert le rôle Localhost.'
            },
            res,
            "Une erreur s'est produite lors de la verification de votre rôle"
        );
    }

    return next();
}
