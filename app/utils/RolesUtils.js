import { ACCOUNTS_ROLES } from '../configs/Properties';

export function isAdmin(userAccount, uniqRole = false) {
    return (
        (!uniqRole || userAccount.roles.length === 1) &&
        userAccount.roles.indexOf(ACCOUNTS_ROLES.ADMIN) !== -1
    );
}

export function isUser(userAccount, uniqRole = false) {
    return (
        (!uniqRole || userAccount.roles.length === 1) &&
        userAccount.roles.indexOf(ACCOUNTS_ROLES.USER || ACCOUNTS_ROLES.USER_SECONDARY) !== -1
    );
}

export function canDoSomething(userAccount) {
    return isAdmin(userAccount) || isUser(userAccount);
}
