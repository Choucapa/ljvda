export default (tokens, req, res) => {
    const status = tokens.status(req, res);

    // get status color
    let color = 0;
    if (status >= 500) color = 31; // red
    if (status >= 400) color = 33; // yellow
    if (status >= 300) color = 36; // cyan
    if (status >= 200) color = 32; // green

    return [
        `\x1b[0m${new Date().toISOString()}`,
        tokens.method(req, res),
        tokens.url(req, res),
        `\x1b[${color}m${tokens.status(req, res)}`,
        `\x1b[0m${tokens['response-time'](req, res)} ms`,
        '-',
        tokens.res(req, res, 'content-length'),
    ].join(' ');
};
