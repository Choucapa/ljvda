/*****************************************************************************
 * Props Validator
 *****************************************************************************/

export default {
    string: '[object String]',
    email: '[object Email]',
    datetime: '[object Date]',
    boolean: '[object Boolean]',
    number: '[object Number]',
    objectJSON: '[object Object]',
    array: '[object Array]',
    arrayNotEmpty: '[object ArrayNonEmpty]',
    null: '[object Null]',
    undefined: '[object Undefined]'
};
