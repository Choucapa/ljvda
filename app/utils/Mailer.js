/*****************************************************************************
 * Mailer Utils
 *****************************************************************************/
// Récupération de Blue Live
import fs from 'fs';
import { map, split, isEmpty } from 'lodash';
import { SERVER, MAIL } from '../configs/ServerConfig';
import { APP_URL_STATIC } from '../configs/Constants';

import { TEMPLATES_MAIL_PROPS } from '../templates/mails';

const SparkPost = require('sparkpost');
const nodemailer = require('nodemailer');

let options = {};

// en production l'api est sur le domaine api.eu.sparkpost.com
// en recette ou dev c'est la valeur par défault api.sparkpost.com
// test
//  if (process.env.NODE_ENV === 'production') options = { origin: 'https://api.eu.sparkpost.com:443' };
let mailjet = {};
let client = {};
if (SERVER.IS_MAILJET && SERVER.MJ_APIKEY_PUBLIC && SERVER.MJ_APIKEY_PRIVATE) {
    mailjet = require('node-mailjet').apiConnect(SERVER.MJ_APIKEY_PUBLIC, SERVER.MJ_APIKEY_PRIVATE);
}
if (SERVER.SPARKPOST_API_KEY) {
    client = new SparkPost(SERVER.SPARKPOST_API_KEY, options);
}
let transporter = nodemailer.createTransport({
    host: MAIL.SMTP_HOST,
    port: MAIL.SMTP_PORT,
    auth: {
        user: MAIL.SMTP_USER,
        password: MAIL.SMTP_PASSWORD
    }
});

/*
 * Send a mail returning a promise
 */
function sendMail(parameters) {
    const destinataires = Array.isArray(parameters.to) ? parameters.to : [parameters.to];
    const recipientsMap = map(destinataires, dest => ({
        address: {
            email: dest,
            name: split(dest, '@')[0]
        }
    }));

    return new Promise(async (resolve, reject) => {
        let ccParams = [];

        if (SERVER.IS_MAILJET) {
            if (parameters.cc !== true && !isEmpty(parameters.cc)) {
                ccParams = [
                    {
                        Email: parameters.cc,
                        Name: split(parameters.cc, '@')[0]
                    }
                ];
            }
            return mailjet
                .post('send', { version: 'v3.1' })
                .request(
                    ccParams
                        ? {
                            Messages: [
                                {
                                    From: {
                                        Email: MAIL.FROM,
                                        Name: 'Anaëlle CLEMENT'
                                    },

                                    To: map(destinataires, dest => ({
                                        Email: dest,
                                        Name: split(dest, '@')[0]
                                    })),
                                    // TextPart: 'Greetings from Mailjet!',
                                    Subject: parameters.subject,
                                    Cc: ccParams,
                                    HTMLPart: parameters.html,
                                    TextPart: parameters.text
                                }
                            ]
                        }
                        : {
                            Messages: [
                                {
                                    From: {
                                        Email: MAIL.FROM,
                                        Name: 'Anaëlle CLEMENT'
                                    },

                                    To: map(destinataires, dest => ({
                                        Email: dest,
                                        Name: split(dest, '@')[0]
                                    })),
                                    // TextPart: 'Greetings from Mailjet!',
                                    Subject: parameters.subject,
                                    HTMLPart: parameters.html,
                                    TextPart: parameters.text
                                }
                            ]
                        }
                )
                .then(result => {
                    resolve(result && result.body);
                })
                .catch(err => {
                    console.error('ERROR : Something went wrong with mailjet', err);
                });
        }
    });
}

/*
 * Send (multiple) mails with a template
 */
export function sendTemplateMails(templateProp, params, to, cc, withAttachment) {
    const { templateHtml, templateTxt, subject } = TEMPLATES_MAIL_PROPS[templateProp];

    const content = [];

    return new Promise((resolve, reject) => {
        // récupération de blue live
        // if (withAttachment) {
        //   const path = __dirname + '/../app/static/mails/bluelive.pdf';
        //   const fileMail = fs.createReadStream(path);
        //   fileMail.on('data', f => {
        //     content.push(f);
        //   });
        //   fileMail.on('end', f => {
        //     console.log('end file');
        //     resolve(content);
        //   });
        // } else {
        resolve();
        // }
    }).then(fileContent => {
        let mailBody = {};
        // récupération de blue live
        // if (withAttachment) {
        //   mailBody = {
        //     attachments: [
        //       {
        //         type: 'application/pdf',
        //         name: 'bluelive.pdf',
        //         data: new Buffer.concat(fileContent).toString('base64')
        //       }
        //     ]
        //   };
        // }
        mailBody = {
            ...mailBody,

            text: templateTxt({
                APP_URL: SERVER.APP_URL,
                APP_URL_FRONT: SERVER.APP_URL_FRONT,
                CONTACT_EMAIL: MAIL.CONTACT_EMAIL,
                APP_URL_STATIC,
                APP_WEBCAL_STATIC: SERVER.APP_WEBCAL_STATIC,
                ...params
            }),
            html: templateHtml({
                APP_URL: SERVER.APP_URL,
                APP_URL_FRONT: SERVER.APP_URL_FRONT,
                CONTACT_EMAIL: MAIL.CONTACT_EMAIL,
                APP_URL_STATIC,
                APP_WEBCAL_STATIC: SERVER.APP_WEBCAL_STATIC,
                ...params
            })
        };

        return sendMail({
            from: MAIL.FROM,
            to: to,
            cc: cc,
            // subject: MAIL.SUBJECT_PREFIX
            //   ? `${MAIL.SUBJECT_PREFIX} - ${subject(params)}`
            //   : subject(params),
            subject: subject(params),
            ...mailBody
        });
    });
}

/*
 * Send a mail with a template
 */
export function sendTemplateMail(templateProp, params, to, cc, withAttachment = false) {
    return sendTemplateMails(templateProp, params, to, cc, withAttachment)
        .then(mailResponse => {
            if (
                !mailResponse ||
                (process.env.NODE_ENV !== 'production' &&
                    SERVER.IS_MAILJET !== 1 &&
                    mailResponse.results.total_accepted_recipients === 0) ||
                (SERVER.IS_MAILJET && mailResponse.Messages[0].Status !== 'success')
            ) {
                console.error('ERROR mailResponse', JSON.stringify(mailResponse));
                throw String('Unable to send the Email');
            } else {
                console.log('Mail envoyé');
            }
        })
        .catch(err => {
            console.error('ERROR sendTemplateMail', err);
            throw String('Unable to send the Email');
        });
}
