/*****************************************************************************
 * Hash Generator
 *****************************************************************************/

import Crypto from 'crypto';
import SHA from 'js-sha512';

/*
 * Generate a Hash
 */
export function generateHash (hashLength = 128, errorMessage = 'Cannot generate a Hash') {
    return new Promise((resolve, reject) => {
        Crypto.randomBytes(hashLength, (err, buf) => {
            if (err) {
                return reject(Error({
                    ...err,
                    statusText: errorMessage,
                }));
            }
            return resolve(buf.toString('hex'));
        });
    });
}

/*
 * Generate the Model access token
 */
export function getModelHash (data) {
    return SHA.sha512(`
    ${data.model_id};
  `).substring(0, 8);
}
