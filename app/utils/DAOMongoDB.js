/*****************************************************************************
 * Data Access Configuration for MongoDB
 *****************************************************************************/

import {MongoClient} from 'mongodb';

import {map, values} from 'lodash';
import {DB} from '../configs/ServerConfig';
import {HAS_AUTOINCREMENT} from '../configs/DataConfig';


const url = `mongodb://anaelle:Azerty@localhost:27017/?authMechanism=DEFAULT`;

function incrementCounter(counterId, db, next) {
    return db
      .collection('jvanaelle_counters')
      .findOneAndUpdate({ _id: counterId }, { $inc: { seq: 1 } }, { new: true, upsert: true }, next);
  }
  
  function incrementCounterMult(counterId, db, nb, next) {
    return db
      .collection('jvanaelle_counters')
      .findOneAndUpdate({ _id: counterId }, { $inc: { seq: nb } }, { new: true, upsert: true }, next);
  }
  

/*
 * getNextSequence:
 * Increment a counter by its ID (name) and return the incremented value
 */
export function getNextSequence(counterId) {

    return new Promise((resolve, reject) => {

            MongoClient.connect(url, {useNewUrlParser: true}, (errConnect, client) => {
                if (errConnect) {
                    return reject(errConnect);
                }
                return incrementCounter(counterId, client.db(DB.DB_NAME), (error, newId) => {

                    if (error) {
                        return reject(error);
                    }
                    client.close();
                    return resolve(newId.value.seq);
                });
            })
        }
    );
}

/*
 * Global Data Access Helpers for GET
return get({
  type: TABLES.XXX,
  select: 'property',
  where: mongo syntax
  orderBy: mongo syntax
  limit: 10,
  offset: 5,
});
 */
export function get(args) {
    const where = args.where ? args.where : {};
    const select = args.select ? args.select : {};
    const orderBy = args.orderBy ? args.orderBy : {};

    let client = null;
    return MongoClient.connect(url, {useNewUrlParser: true})
        .then(db => {
            client = db;
            return new Promise((resolve, reject) =>
                client
                    .db(DB.DB_NAME)
                    .collection(args.type)
                    .find(where)
                    .project(select)
                    .skip(args.offset || 0)
                    .limit(args.limit || 0)
                    .sort(orderBy)
                    .toArray((error, res) => {
                        if (error) {
                            return reject(error);
                        }
                        resolve(res);
                    })
            );
        })
        .then(res => {
            if (args.offset || args.limit) {
                return new Promise((resolve, reject) =>
                    client
                        .db(DB.DB_NAME)
                        .collection(args.type)
                        .countDocuments(where)
                        .then(resCount => {
                            client.close();
                            // il faut retourner le nombre de page max
                            resolve({nbCount: resCount, nbPages: Math.ceil(resCount / args.limit), res: res});
                        })
                );
            } else {
                return res;
            }
        });
}

export function aggregate(args) {
    return new Promise((resolve, reject) =>
        MongoClient.connect(url, {useNewUrlParser: true}, (errConnect, client) => {
            if (errConnect) {
                return reject(errConnect);
            }
            const aggregate = args.aggregate ? args.aggregate : {};
            if (args.numPage || args.size) {
                const pagination = getPagination(args.numPage, args.size);
                args.offset = pagination.offset;
                args.limit = pagination.limit;
            }
            // rajout de la pagination dans l'aggregate :
            if (args.offset || args.limit) {
                aggregate.push(
                    {
                        $facet: {
                            total: [
                                {
                                    $count: 'count'
                                }
                            ],
                            data: [
                                {
                                    $addFields: {
                                        _id: '$_id'
                                    }
                                }
                            ]
                        }
                    },
                    {
                        $unwind: '$total'
                    },

                    {
                        $project: {
                            items: {
                                $slice: [
                                    '$data',
                                    args.offset,
                                    {
                                        $ifNull: [args.limit, '$total.count']
                                    }
                                ]
                            },
                            page: {
                                $literal: args.offset / args.limit + 1
                            },
                            totalPages: {
                                $ceil: {
                                    $divide: ['$total.count', args.limit]
                                }
                            },
                            totalItems: '$total.count'
                        }
                    }
                );
            }
            return client
                .db(DB.DB_NAME)
                .collection(args.type)
                .aggregate(aggregate, {allowDiskUse: true})
                .toArray((error, res) => {
                    if (error) {
                        return reject(error);
                    }
                    client.close();
                    if (args.offset || args.limit) {
                        resolve({
                            nbCount: res[0]?.totalItems || 0,
                            nbPages: res[0]?.totalPages || 0,
                            res: res[0]?.items || []
                        });
                    } else {
                        return resolve(res);
                    }
                });
        })
    );
}

/*
 * Global Data Access Helpers for INSERT
return add({
  type: TABLES.XXX,
  data: data,
});
 */
export function add(args) {
    return new Promise((resolve, reject) =>
        MongoClient.connect(url, {useNewUrlParser: true}, (errConnect, client) => {
            if (errConnect) {
                return reject(errConnect);
            }
            // Get new id value for table
            return incrementCounter(args.type, client.db(DB.DB_NAME), (err, newId) => {
                // do INSERT
                if (HAS_AUTOINCREMENT[args.type]) {
                    args.data[newId.value.name] = newId.value.seq;
                }
                args.data = {
                    ...args.data,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                client
                    .db(DB.DB_NAME)
                    .collection(args.type)
                    .insertOne(args.data, (error, res) => {
                        if (error) {
                            return reject(error);
                        }
                        client.close();
                        if (res.result.ok === 1) {
                            return resolve({
                                affectedRows: res.insertedCount,
                                insertId: res.ops[0][newId.value.name],
                                result: res
                            });
                        }
                        return reject(
                            Error({
                                affectedRows: 0,
                                insertId: undefined,
                                result: res
                            })
                        );
                    });
            });
        })
    );
}

/*
 * Global Data Access Helpers for INSERT Multiple
return addMult({
  type: TABLES.XXX,
  keys: [keys],
  data: [[val], [val]],
});
 */

export function addMult(args, connection = null) {
    return new Promise((resolve, reject) =>
        MongoClient.connect(url, {useNewUrlParser: true}, (errConnect, client) => {
            if (errConnect) {
                return reject(errConnect);
            }

            return incrementCounterMult(
                args.type,
                client.db(DB.DB_NAME),
                values(args.data).length + 1,
                (err, newId) => {
                    // do INSERT
                    let incId = parseInt(newId.value.seq, 10);
                    const ret = map(args.data, (arg, index) => {
                        if (HAS_AUTOINCREMENT[args.type]) {
                            arg[newId.value.name] = incId + (parseInt(index, 10) + 1);
                        }
                        return {
                            ...arg,
                            created_at: new Date(),
                            updated_at: new Date()
                        };
                    });
                    // we need to add data in the bdd
                    return client
                        .db(DB.DB_NAME)
                        .collection(args.type)
                        .insertMany(ret, (error, res) => {
                            if (error) {
                                return reject(error);
                            }
                            client.close();
                            return resolve(res);
                        });
                }
            );
        })
    );
}

/*
 * Global Data Access Helpers for UPDATE
return update({
  type: TABLES.XXX,
  data: data,
  where: { property: [whereArgs] },
});
 */
export function update(args, connection = null) {
    return new Promise((resolve, reject) =>
        MongoClient.connect(url, {useNewUrlParser: true}, (errConnect, client) => {
            if (errConnect) {
                return reject(errConnect);
            }
            args.data = {
                ...args.data,
                updated_at: new Date()
            };
            return client
                .db(DB.DB_NAME)
                .collection(args.type)
                .updateMany(args.where, {[args.action || '$set']: args.data}, (error, res) => {
                    if (error) {
                        return reject(error);
                    }
                    client.close();
                    return resolve({
                        affectedRows: res.result.nModified
                    });
                });
        })
    );
}

/*
 * Global Data Access Helpers for DELETE
return remove({
  type: TABLES.XXX,
  where: { property: [whereArgs] },
});
 */
export function remove(args, connection = null) {
    return new Promise((resolve, reject) =>
        MongoClient.connect(url, {useNewUrlParser: true}, (errConnect, client) => {
            if (errConnect) {
                return reject(errConnect);
            }
            return client
                .db(DB.DB_NAME)
                .collection(args.type)
                .deleteMany(args.where, (error, res) => {
                    if (error) {
                        return reject(error);
                    }
                    client.close();
                    return resolve({
                        affectedRows: res.deletedCount
                    });
                });
        })
    );
}
