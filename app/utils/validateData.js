const options = {
    abortEarly: false, // include all errors
    allowUnknown: true, // ignore unknown props
    stripUnknown: true // remove unknown props
};

export function validateData(schema, data) {
    const { error, value } = schema.validate(data, options);

    if (error) {
        // on fail return comma separated errors
        throw String(
            `Erreur de validation des données: ${error.details.map(x => x.message).join(', ')}`
        );
    } else {
        return true;
    }
}
