import {Router as router} from 'express';

import {sendErrorResponse} from '../utils/ErrorUtils';

import {
    createCommentaire,
    getCommentaires,
    updateCommentaire,
    deleteCommentaire
} from '../controllers/CommentaireController';

/*****************************************************************************
 * Commentaire Routes
 * URL : /commentaires
 *****************************************************************************/

export default router()
    .post('/', (req, res) => {
        Promise.resolve()
            .then(() => createCommentaire(req.body))
            .then(data =>
                res.send({
                    status: 'success',
                    message: 'Commentaire ajouté avec succès',
                    data: data
                })
            )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la création d'un commentaire !"
                )
            );
    })

    .get('/', (req, res) =>
        Promise.resolve()
            .then(() => getCommentaires())
            .then(data => {
                return res.send({
                    status: 'success',
                    message: 'Commentaires récupérés avec succès',
                    data
                });
            })
            .catch(err => {
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la récupération des commentaires !"
                );
            })
    )

    .put('/:id', (req, res) =>
        Promise.resolve()
            .then(() => {
                return updateCommentaire(parseInt(req.params.id, 10), req.body);
            })
            .then(data => {
                res.send({
                    status: 'success',
                    message: 'Commentaire mis à jour avec succès',
                    data: data
                });
            })
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la mise à jour du commentaire"
                )
            )
    )

    .delete('/:id', (req, res) =>
        Promise.resolve()
            .then(() => deleteCommentaire(parseInt(req.params.id, 10)))
            .then(() =>
                res.send({
                    status: 'success',
                    message: 'Commentaire supprimé avec succès'
                })
            )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la suppression du commentaire"
                )
            )
    );
