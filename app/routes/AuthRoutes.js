/*****************************************************************************
 * Auth Routes
 * URL : /auth
 *****************************************************************************/

import { Router as router } from 'express';

import { sendErrorResponse } from '../utils/ErrorUtils';
import {
    loadUser,
    doLogin,
    recoverPassword,
    resetPassword,
    checkEmailExist,
    getRole,
} from '../controllers/AuthController';

export default router()
    .post('/login', (req, res) => {
        Promise.resolve()
            .then(() => doLogin(req.body))
            .then(loginData =>
                res.send({
                    status: 'success',
                    message: 'Successfull login',
                    data: {
                        ...loginData
                    }
                })
            )
            .catch(err =>
                sendErrorResponse(err, res, "Une erreur s'est produite lors de la connexion au compte")
            );
    })

    .get('/me/:userId', (req, res) =>
        Promise.resolve()
            .then(() => loadUser(parseInt(req.params.userId, 10)))
            .then(data =>
                res.send({
                    status: 'success',
                    message: 'Successfull load user',
                    data: data
                })
            )
            .catch(err =>
                sendErrorResponse(err, res, "Une erreur s'est produite lors de la récupération de l'user")
            )
    )

    .put('/password', (req, res) => {
        Promise.resolve()
            .then(() => recoverPassword(req.body))
            .then(data =>
                res.send({
                    status: 'success',
                    message: 'Le mail pour redéfinir votre mot de passe a été envoyé',
                    data: data
                })
            )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de l'envoi du mail"
                )
            );
    })

    .put('/reset-password', (req, res) =>
        Promise.resolve()
            .then(() => resetPassword(req.body))
            .then(() =>
                res.send({
                    status: 'success',
                    message: 'Le nouveau mot de passe a bien été enregistré'
                })
            )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de l'enregistrement du nouveau mot de passe"
                )
            )
    )

    .get('/getRole/:token', (req, res) =>
        Promise.resolve()
            .then(() => getRole(parseInt(req.token, 10)))
            .then(data =>
                res.send({
                    status: 'success',
                    message: 'Successfull get Role',
                    data: data
                })
            )
            .catch(err =>
                sendErrorResponse(err, res, "Une erreur s'est produite lors de la récupération du role de l'utilisateur")
            )
    )

    .post('/checkEmail', (req, res) => {
        Promise.resolve()
            .then(() => checkEmailExist(req.body))
            .then(data =>
                res.send({
                    status: 'success',
                    message: 'Check email réussi',
                    data: data
                })
            )
            .catch(err => {
                sendErrorResponse(err, res);
            });
    });
