import {Router as router} from 'express';

import {sendErrorResponse} from '../utils/ErrorUtils';

import {
    createMembre,
    getMembres,
    updateMembre,
    deleteMembre
} from '../controllers/MembreController';

/*****************************************************************************
 * Membre Routes
 * URL : /membres
 *****************************************************************************/

export default router()
    .post('/', (req, res) => {
        Promise.resolve()
            .then(() => createMembre(req.body))
            .then(data =>
                res.send({
                    status: 'success',
                    message: 'Membre ajouté avec succès',
                    data: data
                })
            )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la création d'un membre !"
                )
            );
    })

    .get('/', (req, res) =>
        Promise.resolve()
            .then(() => getMembres())
            .then(data => {
                return res.send({
                    status: 'success',
                    message: 'Membres récupérés avec succès',
                    data
                });
            })
            .catch(err => {
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la récupération des membres !"
                );
            })
    )

    .put('/:id', (req, res) =>
        Promise.resolve()
            .then(() => {
                return updateMembre(parseInt(req.params.id, 10), req.body);
            })
            .then(data => {
                res.send({
                    status: 'success',
                    message: 'Membre mis à jour avec succès',
                    data: data
                });
            })
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la mise à jour du membre"
                )
            )
    )

    .delete('/:id', (req, res) =>
        Promise.resolve()
            .then(() => deleteMembre(parseInt(req.params.id, 10)))
            .then(() =>
                res.send({
                    status: 'success',
                    message: 'Membre supprimé avec succès'
                })
            )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la suppression du membre"
                )
            )
    );