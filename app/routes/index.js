/*****************************************************************************
 * Applications Routes
 * URL : /api
 *****************************************************************************/

import { Router as router } from 'express';
import AuthRoutes from './AuthRoutes';

import MembresRoutes from './MembreRoutes';
import JeuxRoutes from './JeuxRoutes';
import CommentairesRoutes from './CommentairesRoutes';
import UploadRoutes from './UploadRoutes';


export default router()
    .use('/auth', AuthRoutes)
    .use('/membres', MembresRoutes)
    .use('/jeux', JeuxRoutes)
    .use('/commentaires', CommentairesRoutes)
    .use('/uploads', UploadRoutes)
