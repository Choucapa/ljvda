/*****************************************************************************
 * Uploads Routes
 * URL : /uploads
 *****************************************************************************/

import path from 'path';
import { Router as router } from 'express';
import { sendErrorResponse } from '../utils/ErrorUtils';
import { sendFiles } from '../controllers/UploadController';
import { UPLOAD_DIR } from '../configs/Constants';

export default router()
    .post('/', (req, res) => {
        Promise.resolve()
            .then(() => sendFiles(req.files && req.files.attachment))
            .then((fileName) =>
                res.send({
                    status: 'success',
                    message: 'Photo uploaded successfully',
                    data: {
                        ...fileName
                    }
                })
            )
            .catch((err) => sendErrorResponse(err, res, 'An error occurred when uploading Files'));
    })

    .get('/:fileName', (req, res) =>
        Promise.resolve()
            .then(() => {
                res.type(req.params.fileName);
                return res.sendFile(path.resolve(`${UPLOAD_DIR}/${req.params.fileName}`));
            })
            .catch((err) => sendErrorResponse(err, res, 'An error occurred when getting the File'))
    );
