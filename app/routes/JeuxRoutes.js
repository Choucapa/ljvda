import { Router as router } from 'express';

import { sendErrorResponse } from '../utils/ErrorUtils';

import {
    createJeu,
    getJeux,
    updateJeu,
    deleteJeu,
    getJeuById
} from '../controllers/JeuController';

/*****************************************************************************
 * Jeux Routes
 * URL : /jeux
 *****************************************************************************/



export default router()
    .post('/create', (req, res) => {
        Promise.resolve()
            .then(() =>{console.log("R",req.body) ;createJeu(req.body)})
            .then(data =>        
                res.send({
                    status: 'success',
                    message: 'Jeu ajouté avec succès',
                    data
                })
    )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la création d'un jeu !"
                )
            );
                })

    .get('/', (req, res) => {
        return Promise.resolve()
            .then(() => {
                getJeux()
            })
            .then(data => {
                return res.send({
                    status: 'success',
                    message: 'Jeux récupérés avec succès',
                    data
                });
            })
            .catch(err => {
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la récupération des jeux !"
                );
            })
    })

    .get('/jeu/:id', (req, res) => {
        return Promise.resolve()
            .then(() => {
                console.log("On a rien a faire ici")
            })
            .then(data => {
                return res.send({
                    status: 'success',
                    message: 'Jeu récupéré avec succès',
                    data
                });
            })
            .catch(err => {
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la récupération des jeux !"
                );
            })
    })

    .put('/:id', (req, res) =>
        Promise.resolve()
            .then(() => {
                return updateJeu(parseInt(req.params.id, 10), req.body);
            })
            .then(data => {
                res.send({
                    status: 'success',
                    message: 'Jeu mis à jour avec succès',
                    data: data
                });
            })
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la mise à jour du jeu"
                )
            )
    )

    .delete('/:id', (req, res) =>
        Promise.resolve()
            .then(() => deleteJeu(parseInt(req.params.id, 10)))
            .then(() =>
                res.send({
                    status: 'success',
                    message: 'Jeu supprimé avec succès'
                })
            )
            .catch(err =>
                sendErrorResponse(
                    err,
                    res,
                    "Une erreur s'est produite lors de la suppression du jeu"
                )
            )
    );