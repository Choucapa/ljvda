import accountNewPasswordHtml from './account-new-password.html';
import accountCreationMembreHtml from './account-creation-membre.html';
import accountCreationMembreTxt from './account-creation-membre.txt';
import accountNewPasswordTxt from './account-new-password.txt';


export const TEMPLATES_MAIL = {
    FORGOTTEN_PASSWORD: 'FORGOTTEN_PASSWORD',
    CREATE_ACCOUNT_MEMBRE_PASSWORD: 'CREATE_ACCOUNT_CANDIDAT_PASSWORD',
};

export const TEMPLATES_MAIL_PROPS = {
    [TEMPLATES_MAIL.FORGOTTEN_PASSWORD]: {
        templateHtml: accountNewPasswordHtml,
        templateTxt: accountNewPasswordTxt,
        subject: () => 'Demande de réinitialisation de mon mot de passe !'
    },
    [TEMPLATES_MAIL.CREATE_ACCOUNT_MEMBRE_PASSWORD]: {
        templateHtml: accountCreationMembreHtml,
        templateTxt: accountCreationMembreTxt,
        subject: () => 'Votre compte a été créé !'
    },
};
