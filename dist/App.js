require("source-map-support").install();
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./app/configs/Constants.js":
/*!**********************************!*\
  !*** ./app/configs/Constants.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "APP_URL_STATIC": () => (/* binding */ APP_URL_STATIC),
/* harmony export */   "AUTHORIZED_UPLOAD_TYPES": () => (/* binding */ AUTHORIZED_UPLOAD_TYPES),
/* harmony export */   "DATE_FORMAT": () => (/* binding */ DATE_FORMAT),
/* harmony export */   "UPLOAD_DIR": () => (/* binding */ UPLOAD_DIR),
/* harmony export */   "WKHTMLTOPDF_OPTIONS": () => (/* binding */ WKHTMLTOPDF_OPTIONS)
/* harmony export */ });
/* harmony import */ var _ServerConfig__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ServerConfig */ "./app/configs/ServerConfig.js");
/*****************************************************************************
 * Constants
 *****************************************************************************/

var APP_URL_STATIC = "".concat(_ServerConfig__WEBPACK_IMPORTED_MODULE_0__.SERVER.APP_URL, "/static");
var DATE_FORMAT = 'DD/MM/YYYY';
var UPLOAD_DIR = "".concat(__dirname, "/../public/uploads");
var AUTHORIZED_UPLOAD_TYPES = ['image/gif', 'image/jpeg', 'image/png'];
var WKHTMLTOPDF_OPTIONS = {
  pageSize: 'A4',
  printMediaType: true,
  orientation: 'portrait',
  dpi: 300,
  encoding: 'UTF-8',
  disableSmartShrinking: true,
  T: 16,
  L: 10,
  R: 10,
  B: 20,
  footerHtml: "".concat(APP_URL_STATIC, "/pdf/footer.html"),
  headerHtml: "".concat(APP_URL_STATIC, "/pdf/header.html")
};

/***/ }),

/***/ "./app/configs/DataConfig.js":
/*!***********************************!*\
  !*** ./app/configs/DataConfig.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HAS_AUTOINCREMENT": () => (/* binding */ HAS_AUTOINCREMENT),
/* harmony export */   "PRIMARY_KEYS": () => (/* binding */ PRIMARY_KEYS),
/* harmony export */   "PROPERTIES": () => (/* binding */ PROPERTIES),
/* harmony export */   "TABLES": () => (/* binding */ TABLES)
/* harmony export */ });
/* harmony import */ var _utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/PropsTypes */ "./app/utils/PropsTypes.js");
var _PRIMARY_KEYS, _HAS_AUTOINCREMENT;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*****************************************************************************
 * Data Configuration
 *****************************************************************************/
 // Collections Names

var TABLES = {
  MEMBRES: 'membre',
  JEUX: 'jeux',
  COMMENTAIRES: 'commentaire'
};
/*
 * Primary Keys Names
 */

var PRIMARY_KEYS = (_PRIMARY_KEYS = {}, _defineProperty(_PRIMARY_KEYS, TABLES.JEUX, 'jeu_id'), _defineProperty(_PRIMARY_KEYS, TABLES.MEMBRES, 'membre_id'), _defineProperty(_PRIMARY_KEYS, TABLES.COMMENTAIRES, 'commentaire_id'), _PRIMARY_KEYS);
var PROPERTIES = [];
PROPERTIES[TABLES.MEMBRES] = {
  membre_id: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].number],
  pseudo: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  motdepasse: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  email: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  token: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  created_at: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].datetime]
};
PROPERTIES[TABLES.JEUX] = {
  jeu_id: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].number],
  nomJeu: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  support: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].strin],
  online: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].boolean],
  jaquette: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  created_at: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].datetime],
  description: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string]
};
PROPERTIES[TABLES.COMMENTAIRES] = {
  commentaire_id: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].number],
  jeu_id: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].number],
  membre_id: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].number],
  com: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  note: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].string],
  created_at: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].datetime],
  played: [_utils_PropsTypes__WEBPACK_IMPORTED_MODULE_0__["default"].boolean]
};
var HAS_AUTOINCREMENT = (_HAS_AUTOINCREMENT = {}, _defineProperty(_HAS_AUTOINCREMENT, TABLES.MEMBRES, true), _defineProperty(_HAS_AUTOINCREMENT, TABLES.JEUX, true), _defineProperty(_HAS_AUTOINCREMENT, TABLES.COMMENTAIRES, true), _HAS_AUTOINCREMENT);

/***/ }),

/***/ "./app/configs/Properties.js":
/*!***********************************!*\
  !*** ./app/configs/Properties.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ACCOUNTS_ROLES": () => (/* binding */ ACCOUNTS_ROLES)
/* harmony export */ });
/*****************************************************************************
 * Properties
 *****************************************************************************/

/*
 * ACCOUNTS_ROLES
 */
var ACCOUNTS_ROLES = {
  USER: 'USER',
  ADMIN: 'ADMIN'
};

/***/ }),

/***/ "./app/configs/ServerConfig.js":
/*!*************************************!*\
  !*** ./app/configs/ServerConfig.js ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DB": () => (/* binding */ DB),
/* harmony export */   "MAIL": () => (/* binding */ MAIL),
/* harmony export */   "PDFTK_PATH": () => (/* binding */ PDFTK_PATH),
/* harmony export */   "SERVER": () => (/* binding */ SERVER)
/* harmony export */ });
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! dotenv */ "dotenv");
/* harmony import */ var dotenv__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(dotenv__WEBPACK_IMPORTED_MODULE_1__);
/*****************************************************************************
 * Server Configuration
 *****************************************************************************/


dotenv__WEBPACK_IMPORTED_MODULE_1___default().config( true && !(0,fs__WEBPACK_IMPORTED_MODULE_0__.existsSync)('./.env') ? {
  path: './.env.example'
} : null);
/*
 * API Server Configuration
 */

var SERVER = {
  APP_PORT: process.env.APP_PORT,
  APP_URL: process.env.APP_URL,
  APP_URL_FRONT: process.env.APP_URL_FRONT || process.env.APP_URL,
  SPARKPOST_API_KEY: process.env.SPARKPOST_API_KEY,
  LOCALHOST_TOKEN: process.env.LOCALHOST_TOKEN,
  MJ_APIKEY_PUBLIC: process.env.MJ_APIKEY_PUBLIC,
  MJ_APIKEY_PRIVATE: process.env.MJ_APIKEY_PRIVATE,
  IS_MAILJET: process.env.IS_MAILJET && Number(process.env.IS_MAILJET) || 0
};
/*
 * Database Configuration
 */

var DB = {
  DB_HOST: process.env.DB_HOST,
  DB_PORT: process.env.DB_PORT,
  DB_USER: process.env.DB_USER,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_NAME: process.env.DB_NAME,
  DB_EXTRA: process.env.DB_EXTRA || {}
};
/*
 * MAIL Configuration
 */

var MAIL = {
  FROM: process.env.FROM,
  SUBJECT_PREFIX: process.env.SUBJECT_PREFIX,
  CONTACT_EMAIL: process.env.CONTACT_EMAIL
};
/*
 * Other Configuration
 */

var PDFTK_PATH = process.env.PDFTK_PATH || 'pdftk';


/***/ }),

/***/ "./app/controllers/AuthController.js":
/*!*******************************************!*\
  !*** ./app/controllers/AuthController.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "checkEmailExist": () => (/* binding */ checkEmailExist),
/* harmony export */   "doLogin": () => (/* binding */ doLogin),
/* harmony export */   "doLoginAdmin": () => (/* binding */ doLoginAdmin),
/* harmony export */   "getRole": () => (/* binding */ getRole),
/* harmony export */   "loadUser": () => (/* binding */ loadUser),
/* harmony export */   "recoverPassword": () => (/* binding */ recoverPassword),
/* harmony export */   "resetPassword": () => (/* binding */ resetPassword)
/* harmony export */ });
/* harmony import */ var js_sha512__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-sha512 */ "js-sha512");
/* harmony import */ var js_sha512__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_sha512__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _utils_DAO__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/DAO */ "./app/utils/DAO.js");
/* harmony import */ var _utils_validateData__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/validateData */ "./app/utils/validateData.js");
/* harmony import */ var _utils_HashGenerator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils/HashGenerator */ "./app/utils/HashGenerator.js");
/* harmony import */ var _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../configs/DataConfig */ "./app/configs/DataConfig.js");
/* harmony import */ var _utils_Mailer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../utils/Mailer */ "./app/utils/Mailer.js");
/* harmony import */ var _templates_mails_index__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../templates/mails/index */ "./app/templates/mails/index.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*****************************************************************************
 * Account Controller
 * Manage Accounts
 *****************************************************************************/



moment__WEBPACK_IMPORTED_MODULE_2___default().locale('fr');







var Joi = __webpack_require__(/*! joi */ "joi");
/*
 * Do Login
 * Return: The Authorization Token
 */


function loadUser(userId) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
    select: {
      membre_id: true,
      pseudo: true,
      email: true,
      role: true
    },
    where: {
      membre_id: userId
    }
  }).then(function (rows) {
    if (rows.length === 0) {
      throw String("Impossible de récupérer l'user");
    }

    return rows[0];
  });
}
function doLogin(data) {
  if (!data.password) {
    throw String('Mail inconnu ou mauvais mot de passe');
  }

  var loginData = {
    membre_id: '',
    authorization_token: '',
    reset_token: ''
  };
  var userData;
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
    select: {
      membre_id: true,
      pseudo: true,
      email: true,
      role: true
    },
    where: {
      email: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.toLower)(data.email),
      password: js_sha512__WEBPACK_IMPORTED_MODULE_0___default().sha512(data.password)
    }
  }).then(function (rows) {
    if (!rows || rows.length !== 1) {
      throw String('Mail inconnu ou mauvais mot de passe');
    }

    loginData.membre_id = rows[0].membre_id;
    userData = (0,lodash__WEBPACK_IMPORTED_MODULE_1__.omit)(rows[0], ['password']);
  }).then(function (res) {
    return (0,_utils_HashGenerator__WEBPACK_IMPORTED_MODULE_5__.generateHash)(undefined, 'Cannot generate an token');
  }).then(function (authorizationToken) {
    loginData.token = authorizationToken;
    return _objectSpread(_objectSpread({}, loginData), userData);
  });
}
/*
 * Do Login Admin
 * Return: The Authorization Token
 */

function doLoginAdmin(data) {
  if (!data.password) {
    throw String('Mail inconnu ou mauvais mot de passe');
  }

  var loginData = {
    membre_id: '',
    authorization_token: '',
    reset_token: ''
  };
  var userData;
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
    where: {
      email: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.toLower)(data.email),
      password: js_sha512__WEBPACK_IMPORTED_MODULE_0___default().sha512(data.password),
      role: {
        $in: ['ADMIN']
      }
    }
  }).then(function (rows) {
    if (!rows || rows.length !== 1) {
      throw String('Mail inconnu ou mauvais mot de passe');
    }

    loginData.membre_id = rows[0].membre_id;
    userData = rows[0];
  }).then(function (res) {
    return (0,_utils_HashGenerator__WEBPACK_IMPORTED_MODULE_5__.generateHash)(undefined, 'Cannot generate an Authorization token');
  }).then(function (authorizationToken) {
    loginData.token = authorizationToken;
    return _objectSpread(_objectSpread({}, loginData), userData);
  });
}
/*
 * recoverPassword
 * Return: matching user with new password
 */

function recoverPassword(data) {
  var usersData = {};
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
    select: {
      membre_id: true,
      pseudo: true,
      email: true,
      role: true
    },
    where: {
      email: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.toLower)(data.email)
    }
  }).then(function (rows) {
    if (rows.length === 0) {
      throw String('Email non trouvé');
    }

    usersData = rows[0];
    return (0,_utils_HashGenerator__WEBPACK_IMPORTED_MODULE_5__.generateHash)(undefined, 'Cannot generate a Reset token');
  }).then(function (generatedHash) {
    usersData.token = generatedHash;
    return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.update)({
      type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
      data: {
        reset_token: js_sha512__WEBPACK_IMPORTED_MODULE_0___default().sha512(usersData.token)
      },
      where: {
        membre_id: usersData.membre_id
      }
    });
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Unable to update the Reset token');
    }

    return (0,_utils_Mailer__WEBPACK_IMPORTED_MODULE_7__.sendTemplateMail)(_templates_mails_index__WEBPACK_IMPORTED_MODULE_8__.TEMPLATES_MAIL.FORGOTTEN_PASSWORD, {
      user: usersData,
      user_id: usersData.user_id
    }, usersData.email, {}, false), usersData.role;
  });
}
function resetPassword(data) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
    where: {
      email: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.toLower)(data.email),
      membre_id: data.membre_id
    }
  }).then(function (rows) {
    if (rows.length !== 1) {
      throw String('Impossible de récupérer le compte');
    }

    return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.update)({
      type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
      data: {
        password: js_sha512__WEBPACK_IMPORTED_MODULE_0___default().sha512(data.password)
      },
      where: {
        membre_id: rows[0].membre_id
      }
    });
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Impossible de mettre à jour le mot de passe');
    }
  });
}
function checkEmailExist(data) {
  var schema = Joi.object({
    email: Joi.string().email({
      minDomainSegments: 2
    }).required()
  });

  if ((0,_utils_validateData__WEBPACK_IMPORTED_MODULE_4__.validateData)(schema, data)) {
    return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.get)({
      type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
      where: {
        email: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.toLower)(data.email)
      }
    }).then(function (rows) {
      if (rows.length === 1) {
        throw 'Cette adresse e-mail est déja utilisée par un autre compte';
      }
    });
  }
}
function getRole(authorization_token) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_3__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_6__.TABLES.MEMBRES,
    where: {
      authorization_token: authorization_token
    },
    select: {
      role: 1,
      email: 1
    }
  }).then(function (rows) {
    if (rows.length === 0) {
      throw String('Impossible de récupérer le role');
    }

    return rows[0];
  });
}

/***/ }),

/***/ "./app/controllers/CommentaireController.js":
/*!**************************************************!*\
  !*** ./app/controllers/CommentaireController.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createCommentaire": () => (/* binding */ createCommentaire),
/* harmony export */   "deleteCommentaire": () => (/* binding */ deleteCommentaire),
/* harmony export */   "getCommentaires": () => (/* binding */ getCommentaires),
/* harmony export */   "updateCommentaire": () => (/* binding */ updateCommentaire)
/* harmony export */ });
/* harmony import */ var _utils_DAO__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/DAO */ "./app/utils/DAO.js");
/* harmony import */ var _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../configs/DataConfig */ "./app/configs/DataConfig.js");
/* harmony import */ var _utils_validateData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/validateData */ "./app/utils/validateData.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var Joi = __webpack_require__(/*! joi */ "joi");

function getCommentaires() {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.COMMENTAIRES
  });
}
function createCommentaire(data) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.add)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.COMMENTAIRES,
    data: data
  }).then(function (rows) {
    return _objectSpread(_objectSpread({}, data), {}, {
      commentaire_id: rows.insertId
    });
  });
}
function updateCommentaire(id, data) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.update)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.COMMENTAIRES,
    data: data,
    where: {
      commentaire_id: id
    }
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Impossible de mettre à jour le commentaire');
    }

    return _objectSpread({
      commentaire_id: id
    }, data);
  });
}
function deleteCommentaire(id) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.remove)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.COMMENTAIRES,
    where: {
      commentaire_id: id
    }
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Impossible de supprimer le commentaire');
    }
  });
}

/***/ }),

/***/ "./app/controllers/JeuController.js":
/*!******************************************!*\
  !*** ./app/controllers/JeuController.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createJeu": () => (/* binding */ createJeu),
/* harmony export */   "deleteJeu": () => (/* binding */ deleteJeu),
/* harmony export */   "getJeuById": () => (/* binding */ getJeuById),
/* harmony export */   "getJeux": () => (/* binding */ getJeux),
/* harmony export */   "updateJeu": () => (/* binding */ updateJeu)
/* harmony export */ });
/* harmony import */ var _utils_DAO__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/DAO */ "./app/utils/DAO.js");
/* harmony import */ var _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../configs/DataConfig */ "./app/configs/DataConfig.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




var Joi = __webpack_require__(/*! joi */ "joi");

function getJeux() {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.JEUX
  });
}
function getJeuById(id) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.JEUX,
    where: {
      _id: id
    }
  }).then(function (res) {
    return console.log("test", res);
  });
}
function createJeu(data) {
  console.log("GOOD JOB", data);
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.add)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.JEUX,
    data: data
  }).then(function (rows) {
    return _objectSpread(_objectSpread({}, data), {}, {
      jeu_id: rows.insertId
    });
  });
}
function updateJeu(id, data) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.update)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.JEUX,
    data: data,
    where: {
      jeu_id: id
    }
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Impossible de mettre à jour le jeu');
    }

    return _objectSpread({
      jeu_id: id
    }, data);
  });
}
function deleteJeu(id) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.remove)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.JEUX,
    where: {
      jeu_id: id
    }
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Impossible de supprimer le jeu');
    }
  });
}

/***/ }),

/***/ "./app/controllers/MembreController.js":
/*!*********************************************!*\
  !*** ./app/controllers/MembreController.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "createMembre": () => (/* binding */ createMembre),
/* harmony export */   "deleteMembre": () => (/* binding */ deleteMembre),
/* harmony export */   "getMembres": () => (/* binding */ getMembres),
/* harmony export */   "updateMembre": () => (/* binding */ updateMembre)
/* harmony export */ });
/* harmony import */ var _utils_DAO__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/DAO */ "./app/utils/DAO.js");
/* harmony import */ var _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../configs/DataConfig */ "./app/configs/DataConfig.js");
/* harmony import */ var _utils_validateData__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/validateData */ "./app/utils/validateData.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var Joi = __webpack_require__(/*! joi */ "joi");

function getMembres() {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.get)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.MEMBRES
  });
}
function createMembre(data) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.add)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.MEMBRES,
    data: data
  }).then(function (rows) {
    return _objectSpread(_objectSpread({}, data), {}, {
      jeu_id: rows.insertId
    });
  });
}
function updateMembre(id, data) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.update)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.MEMBRES,
    data: data,
    where: {
      jeu_id: id
    }
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Impossible de mettre à jour le jeu');
    }

    return _objectSpread({
      jeu_id: id
    }, data);
  });
}
function deleteMembre(id) {
  return (0,_utils_DAO__WEBPACK_IMPORTED_MODULE_0__.remove)({
    type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_1__.TABLES.MEMBRES,
    where: {
      jeu_id: id
    }
  }).then(function (rows) {
    if (rows.affectedRows !== 1) {
      throw String('Impossible de supprimer le jeu');
    }
  });
}

/***/ }),

/***/ "./app/controllers/UploadController.js":
/*!*********************************************!*\
  !*** ./app/controllers/UploadController.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "deleteFiles": () => (/* binding */ deleteFiles),
/* harmony export */   "sendFiles": () => (/* binding */ sendFiles),
/* harmony export */   "sendFilesWithDB": () => (/* binding */ sendFilesWithDB)
/* harmony export */ });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var slug__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! slug */ "slug");
/* harmony import */ var slug__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(slug__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _configs_Constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../configs/Constants */ "./app/configs/Constants.js");
/* harmony import */ var _utils_StringUtils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils/StringUtils */ "./app/utils/StringUtils.js");
/*****************************************************************************
 * Upload Controller
 * Manage Uploads
 *****************************************************************************/






/*
 * Send Files
 * Return: The URLs of created Files
 */

function sendFiles(files, fileCallback) {
  console.log('Send Files', files);

  if (!files) {
    throw String('No files to send');
  } else if (!Array.isArray(files)) {
    files = [files];
  }

  var acceptedFiles = files.filter(function (file) {
    return file.name && file.data && file.mimetype && _configs_Constants__WEBPACK_IMPORTED_MODULE_4__.AUTHORIZED_UPLOAD_TYPES.indexOf(file.mimetype) >= 0;
  });
  return Promise.all((0,lodash__WEBPACK_IMPORTED_MODULE_2__.map)(acceptedFiles, function (file) {
    return new Promise(function (resolve, reject) {
      var fileName = "".concat(moment__WEBPACK_IMPORTED_MODULE_0___default()().format('YYYYMMDD-HHmmss'), "-").concat(slug__WEBPACK_IMPORTED_MODULE_3___default()((0,_utils_StringUtils__WEBPACK_IMPORTED_MODULE_5__.removeExtension)(file.name), (slug__WEBPACK_IMPORTED_MODULE_3___default().defaults.modes.rfc3986))).concat((0,_utils_StringUtils__WEBPACK_IMPORTED_MODULE_5__.getExtension)(file.name));
      file.mv("".concat(_configs_Constants__WEBPACK_IMPORTED_MODULE_4__.UPLOAD_DIR, "/").concat(fileName), function (err) {
        if (err) {
          console.error(err);
          reject(String("Unable to move the file: '".concat(file.name, "'")));
        }

        return resolve(fileName);
      });
    }).then(function (fileName) {
      return typeof fileCallback === 'function' ? fileCallback({
        file: file,
        fileName: fileName
      }) : fileName;
    });
  }));
}
/*
 * Send Files and add row in DB
 * Return: The URLs of created Files
 */

function sendFilesWithDB(files) {
  return sendFiles(files).then(function (res) {
    var fileName = res[0];
    return {
      fileName: fileName,
      fileType: files.mimetype
    };
  });
}
/*
 * Delete Files
 */

function deleteFiles(fileNames, fileCallback) {
  if (!Array.isArray(fileNames)) {
    fileNames = [fileNames];
  }

  return Promise.all((0,lodash__WEBPACK_IMPORTED_MODULE_2__.map)(fileNames, function (fileName) {
    return new Promise(function (resolve, reject) {
      fs__WEBPACK_IMPORTED_MODULE_1___default().unlink("".concat(_configs_Constants__WEBPACK_IMPORTED_MODULE_4__.UPLOAD_DIR, "/").concat(fileName), function (err) {
        if (err) {
          console.error(err);
          reject(String("Impossible de supprimer le fichier: '".concat(fileName, "'")));
        }

        return resolve(fileName);
      });
    }).then(function () {
      return typeof fileCallback === 'function' ? fileCallback(fileName) : fileName;
    });
  }));
}

/***/ }),

/***/ "./app/routes/AuthRoutes.js":
/*!**********************************!*\
  !*** ./app/routes/AuthRoutes.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/ErrorUtils */ "./app/utils/ErrorUtils.js");
/* harmony import */ var _controllers_AuthController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../controllers/AuthController */ "./app/controllers/AuthController.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*****************************************************************************
 * Auth Routes
 * URL : /auth
 *****************************************************************************/



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,express__WEBPACK_IMPORTED_MODULE_0__.Router)().post('/login', function (req, res) {
  Promise.resolve().then(function () {
    return (0,_controllers_AuthController__WEBPACK_IMPORTED_MODULE_2__.doLogin)(req.body);
  }).then(function (loginData) {
    return res.send({
      status: 'success',
      message: 'Successfull login',
      data: _objectSpread({}, loginData)
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la connexion au compte");
  });
}).get('/me/:userId', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_AuthController__WEBPACK_IMPORTED_MODULE_2__.loadUser)(parseInt(req.params.userId, 10));
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Successfull load user',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la récupération de l'user");
  });
}).put('/password', function (req, res) {
  Promise.resolve().then(function () {
    return (0,_controllers_AuthController__WEBPACK_IMPORTED_MODULE_2__.recoverPassword)(req.body);
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Le mail pour redéfinir votre mot de passe a été envoyé',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de l'envoi du mail");
  });
}).put('/reset-password', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_AuthController__WEBPACK_IMPORTED_MODULE_2__.resetPassword)(req.body);
  }).then(function () {
    return res.send({
      status: 'success',
      message: 'Le nouveau mot de passe a bien été enregistré'
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de l'enregistrement du nouveau mot de passe");
  });
}).get('/getRole/:token', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_AuthController__WEBPACK_IMPORTED_MODULE_2__.getRole)(parseInt(req.token, 10));
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Successfull get Role',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la récupération du role de l'utilisateur");
  });
}).post('/checkEmail', function (req, res) {
  Promise.resolve().then(function () {
    return (0,_controllers_AuthController__WEBPACK_IMPORTED_MODULE_2__.checkEmailExist)(req.body);
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Check email réussi',
      data: data
    });
  })["catch"](function (err) {
    (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res);
  });
}));

/***/ }),

/***/ "./app/routes/CommentairesRoutes.js":
/*!******************************************!*\
  !*** ./app/routes/CommentairesRoutes.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/ErrorUtils */ "./app/utils/ErrorUtils.js");
/* harmony import */ var _controllers_CommentaireController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../controllers/CommentaireController */ "./app/controllers/CommentaireController.js");



/*****************************************************************************
 * Commentaire Routes
 * URL : /commentaires
 *****************************************************************************/

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,express__WEBPACK_IMPORTED_MODULE_0__.Router)().post('/', function (req, res) {
  Promise.resolve().then(function () {
    return (0,_controllers_CommentaireController__WEBPACK_IMPORTED_MODULE_2__.createCommentaire)(req.body);
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Commentaire ajouté avec succès',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la création d'un commentaire !");
  });
}).get('/', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_CommentaireController__WEBPACK_IMPORTED_MODULE_2__.getCommentaires)();
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Commentaires récupérés avec succès',
      data: data
    });
  })["catch"](function (err) {
    (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la récupération des commentaires !");
  });
}).put('/:id', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_CommentaireController__WEBPACK_IMPORTED_MODULE_2__.updateCommentaire)(parseInt(req.params.id, 10), req.body);
  }).then(function (data) {
    res.send({
      status: 'success',
      message: 'Commentaire mis à jour avec succès',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la mise à jour du commentaire");
  });
})["delete"]('/:id', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_CommentaireController__WEBPACK_IMPORTED_MODULE_2__.deleteCommentaire)(parseInt(req.params.id, 10));
  }).then(function () {
    return res.send({
      status: 'success',
      message: 'Commentaire supprimé avec succès'
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la suppression du commentaire");
  });
}));

/***/ }),

/***/ "./app/routes/JeuxRoutes.js":
/*!**********************************!*\
  !*** ./app/routes/JeuxRoutes.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/ErrorUtils */ "./app/utils/ErrorUtils.js");
/* harmony import */ var _controllers_JeuController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../controllers/JeuController */ "./app/controllers/JeuController.js");



/*****************************************************************************
 * Jeux Routes
 * URL : /jeux
 *****************************************************************************/

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,express__WEBPACK_IMPORTED_MODULE_0__.Router)().post('/create', function (req, res) {
  Promise.resolve().then(function () {
    console.log("R", req.body);
    (0,_controllers_JeuController__WEBPACK_IMPORTED_MODULE_2__.createJeu)(req.body);
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Jeu ajouté avec succès',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la création d'un jeu !");
  });
}).get('/', function (req, res) {
  return Promise.resolve().then(function () {
    (0,_controllers_JeuController__WEBPACK_IMPORTED_MODULE_2__.getJeux)();
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Jeux récupérés avec succès',
      data: data
    });
  })["catch"](function (err) {
    (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la récupération des jeux !");
  });
}).get('/jeu/:id', function (req, res) {
  return Promise.resolve().then(function () {
    console.log("On a rien a faire ici");
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Jeu récupéré avec succès',
      data: data
    });
  })["catch"](function (err) {
    (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la récupération des jeux !");
  });
}).put('/:id', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_JeuController__WEBPACK_IMPORTED_MODULE_2__.updateJeu)(parseInt(req.params.id, 10), req.body);
  }).then(function (data) {
    res.send({
      status: 'success',
      message: 'Jeu mis à jour avec succès',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la mise à jour du jeu");
  });
})["delete"]('/:id', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_JeuController__WEBPACK_IMPORTED_MODULE_2__.deleteJeu)(parseInt(req.params.id, 10));
  }).then(function () {
    return res.send({
      status: 'success',
      message: 'Jeu supprimé avec succès'
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la suppression du jeu");
  });
}));

/***/ }),

/***/ "./app/routes/MembreRoutes.js":
/*!************************************!*\
  !*** ./app/routes/MembreRoutes.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/ErrorUtils */ "./app/utils/ErrorUtils.js");
/* harmony import */ var _controllers_MembreController__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../controllers/MembreController */ "./app/controllers/MembreController.js");



/*****************************************************************************
 * Membre Routes
 * URL : /membres
 *****************************************************************************/

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,express__WEBPACK_IMPORTED_MODULE_0__.Router)().post('/', function (req, res) {
  Promise.resolve().then(function () {
    return (0,_controllers_MembreController__WEBPACK_IMPORTED_MODULE_2__.createMembre)(req.body);
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Membre ajouté avec succès',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la création d'un membre !");
  });
}).get('/', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_MembreController__WEBPACK_IMPORTED_MODULE_2__.getMembres)();
  }).then(function (data) {
    return res.send({
      status: 'success',
      message: 'Membres récupérés avec succès',
      data: data
    });
  })["catch"](function (err) {
    (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la récupération des membres !");
  });
}).put('/:id', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_MembreController__WEBPACK_IMPORTED_MODULE_2__.updateMembre)(parseInt(req.params.id, 10), req.body);
  }).then(function (data) {
    res.send({
      status: 'success',
      message: 'Membre mis à jour avec succès',
      data: data
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la mise à jour du membre");
  });
})["delete"]('/:id', function (req, res) {
  return Promise.resolve().then(function () {
    return (0,_controllers_MembreController__WEBPACK_IMPORTED_MODULE_2__.deleteMembre)(parseInt(req.params.id, 10));
  }).then(function () {
    return res.send({
      status: 'success',
      message: 'Membre supprimé avec succès'
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_1__.sendErrorResponse)(err, res, "Une erreur s'est produite lors de la suppression du membre");
  });
}));

/***/ }),

/***/ "./app/routes/UploadRoutes.js":
/*!************************************!*\
  !*** ./app/routes/UploadRoutes.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! path */ "path");
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/ErrorUtils */ "./app/utils/ErrorUtils.js");
/* harmony import */ var _controllers_UploadController__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../controllers/UploadController */ "./app/controllers/UploadController.js");
/* harmony import */ var _configs_Constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../configs/Constants */ "./app/configs/Constants.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*****************************************************************************
 * Uploads Routes
 * URL : /uploads
 *****************************************************************************/





/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,express__WEBPACK_IMPORTED_MODULE_1__.Router)().post('/', function (req, res) {
  Promise.resolve().then(function () {
    return (0,_controllers_UploadController__WEBPACK_IMPORTED_MODULE_3__.sendFiles)(req.files && req.files.attachment);
  }).then(function (fileName) {
    return res.send({
      status: 'success',
      message: 'Photo uploaded successfully',
      data: _objectSpread({}, fileName)
    });
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_2__.sendErrorResponse)(err, res, 'An error occurred when uploading Files');
  });
}).get('/:fileName', function (req, res) {
  return Promise.resolve().then(function () {
    res.type(req.params.fileName);
    return res.sendFile(path__WEBPACK_IMPORTED_MODULE_0___default().resolve("".concat(_configs_Constants__WEBPACK_IMPORTED_MODULE_4__.UPLOAD_DIR, "/").concat(req.params.fileName)));
  })["catch"](function (err) {
    return (0,_utils_ErrorUtils__WEBPACK_IMPORTED_MODULE_2__.sendErrorResponse)(err, res, 'An error occurred when getting the File');
  });
}));

/***/ }),

/***/ "./app/routes/index.js":
/*!*****************************!*\
  !*** ./app/routes/index.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _AuthRoutes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AuthRoutes */ "./app/routes/AuthRoutes.js");
/* harmony import */ var _MembreRoutes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./MembreRoutes */ "./app/routes/MembreRoutes.js");
/* harmony import */ var _JeuxRoutes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./JeuxRoutes */ "./app/routes/JeuxRoutes.js");
/* harmony import */ var _CommentairesRoutes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./CommentairesRoutes */ "./app/routes/CommentairesRoutes.js");
/* harmony import */ var _UploadRoutes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./UploadRoutes */ "./app/routes/UploadRoutes.js");
/*****************************************************************************
 * Applications Routes
 * URL : /api
 *****************************************************************************/






/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,express__WEBPACK_IMPORTED_MODULE_0__.Router)().use('/auth', _AuthRoutes__WEBPACK_IMPORTED_MODULE_1__["default"]).use('/membres', _MembreRoutes__WEBPACK_IMPORTED_MODULE_2__["default"]).use('/jeux', _JeuxRoutes__WEBPACK_IMPORTED_MODULE_3__["default"]).use('/commentaires', _CommentairesRoutes__WEBPACK_IMPORTED_MODULE_4__["default"]).use('/uploads', _UploadRoutes__WEBPACK_IMPORTED_MODULE_5__["default"]));

/***/ }),

/***/ "./app/templates/mails/index.js":
/*!**************************************!*\
  !*** ./app/templates/mails/index.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TEMPLATES_MAIL": () => (/* binding */ TEMPLATES_MAIL),
/* harmony export */   "TEMPLATES_MAIL_PROPS": () => (/* binding */ TEMPLATES_MAIL_PROPS)
/* harmony export */ });
/* harmony import */ var _account_new_password_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./account-new-password.html */ "./app/templates/mails/account-new-password.html");
/* harmony import */ var _account_new_password_html__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_account_new_password_html__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _account_creation_membre_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./account-creation-membre.html */ "./app/templates/mails/account-creation-membre.html");
/* harmony import */ var _account_creation_membre_html__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_account_creation_membre_html__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _account_creation_membre_txt__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./account-creation-membre.txt */ "./app/templates/mails/account-creation-membre.txt");
/* harmony import */ var _account_creation_membre_txt__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_account_creation_membre_txt__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _account_new_password_txt__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./account-new-password.txt */ "./app/templates/mails/account-new-password.txt");
/* harmony import */ var _account_new_password_txt__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_account_new_password_txt__WEBPACK_IMPORTED_MODULE_3__);
var _TEMPLATES_MAIL_PROPS;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var TEMPLATES_MAIL = {
  FORGOTTEN_PASSWORD: 'FORGOTTEN_PASSWORD',
  CREATE_ACCOUNT_MEMBRE_PASSWORD: 'CREATE_ACCOUNT_CANDIDAT_PASSWORD'
};
var TEMPLATES_MAIL_PROPS = (_TEMPLATES_MAIL_PROPS = {}, _defineProperty(_TEMPLATES_MAIL_PROPS, TEMPLATES_MAIL.FORGOTTEN_PASSWORD, {
  templateHtml: (_account_new_password_html__WEBPACK_IMPORTED_MODULE_0___default()),
  templateTxt: (_account_new_password_txt__WEBPACK_IMPORTED_MODULE_3___default()),
  subject: function subject() {
    return 'Demande de réinitialisation de mon mot de passe !';
  }
}), _defineProperty(_TEMPLATES_MAIL_PROPS, TEMPLATES_MAIL.CREATE_ACCOUNT_MEMBRE_PASSWORD, {
  templateHtml: (_account_creation_membre_html__WEBPACK_IMPORTED_MODULE_1___default()),
  templateTxt: (_account_creation_membre_txt__WEBPACK_IMPORTED_MODULE_2___default()),
  subject: function subject() {
    return 'Votre compte a été créé !';
  }
}), _TEMPLATES_MAIL_PROPS);

/***/ }),

/***/ "./app/utils/DAO.js":
/*!**************************!*\
  !*** ./app/utils/DAO.js ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "add": () => (/* reexport safe */ _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__.add),
/* harmony export */   "addMult": () => (/* reexport safe */ _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__.addMult),
/* harmony export */   "aggregate": () => (/* reexport safe */ _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__.aggregate),
/* harmony export */   "get": () => (/* reexport safe */ _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__.get),
/* harmony export */   "getNextSequence": () => (/* reexport safe */ _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__.getNextSequence),
/* harmony export */   "remove": () => (/* reexport safe */ _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__.remove),
/* harmony export */   "update": () => (/* reexport safe */ _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__.update)
/* harmony export */ });
/* harmony import */ var _DAOMongoDB__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DAOMongoDB */ "./app/utils/DAOMongoDB.js");
/*****************************************************************************
 * Default Data Access Configuration
 *****************************************************************************/
// export * from './DAOMySQL';


/***/ }),

/***/ "./app/utils/DAOMongoDB.js":
/*!*********************************!*\
  !*** ./app/utils/DAOMongoDB.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "add": () => (/* binding */ add),
/* harmony export */   "addMult": () => (/* binding */ addMult),
/* harmony export */   "aggregate": () => (/* binding */ aggregate),
/* harmony export */   "get": () => (/* binding */ get),
/* harmony export */   "getNextSequence": () => (/* binding */ getNextSequence),
/* harmony export */   "remove": () => (/* binding */ remove),
/* harmony export */   "update": () => (/* binding */ update)
/* harmony export */ });
/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongodb */ "mongodb");
/* harmony import */ var mongodb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongodb__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../configs/ServerConfig */ "./app/configs/ServerConfig.js");
/* harmony import */ var _configs_DataConfig__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../configs/DataConfig */ "./app/configs/DataConfig.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*****************************************************************************
 * Data Access Configuration for MongoDB
 *****************************************************************************/




var url = "mongodb://anaelle:Azerty@localhost:27017/?authMechanism=DEFAULT";

function incrementCounter(counterId, db, next) {
  return db.collection('jvanaelle_counters').findOneAndUpdate({
    _id: counterId
  }, {
    $inc: {
      seq: 1
    }
  }, {
    "new": true,
    upsert: true
  }, next);
}

function incrementCounterMult(counterId, db, nb, next) {
  return db.collection('jvanaelle_counters').findOneAndUpdate({
    _id: counterId
  }, {
    $inc: {
      seq: nb
    }
  }, {
    "new": true,
    upsert: true
  }, next);
}
/*
 * getNextSequence:
 * Increment a counter by its ID (name) and return the incremented value
 */


function getNextSequence(counterId) {
  return new Promise(function (resolve, reject) {
    mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(url, {
      useNewUrlParser: true
    }, function (errConnect, client) {
      if (errConnect) {
        return reject(errConnect);
      }

      return incrementCounter(counterId, client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME), function (error, newId) {
        if (error) {
          return reject(error);
        }

        client.close();
        return resolve(newId.value.seq);
      });
    });
  });
}
/*
 * Global Data Access Helpers for GET
return get({
  type: TABLES.XXX,
  select: 'property',
  where: mongo syntax
  orderBy: mongo syntax
  limit: 10,
  offset: 5,
});
 */

function get(args) {
  var where = args.where ? args.where : {};
  var select = args.select ? args.select : {};
  var orderBy = args.orderBy ? args.orderBy : {};
  var client = null;
  return mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(url, {
    useNewUrlParser: true
  }).then(function (db) {
    client = db;
    return new Promise(function (resolve, reject) {
      return client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME).collection(args.type).find(where).project(select).skip(args.offset || 0).limit(args.limit || 0).sort(orderBy).toArray(function (error, res) {
        if (error) {
          return reject(error);
        }

        resolve(res);
      });
    });
  }).then(function (res) {
    if (args.offset || args.limit) {
      return new Promise(function (resolve, reject) {
        return client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME).collection(args.type).countDocuments(where).then(function (resCount) {
          client.close(); // il faut retourner le nombre de page max

          resolve({
            nbCount: resCount,
            nbPages: Math.ceil(resCount / args.limit),
            res: res
          });
        });
      });
    } else {
      return res;
    }
  });
}
function aggregate(args) {
  return new Promise(function (resolve, reject) {
    return mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(url, {
      useNewUrlParser: true
    }, function (errConnect, client) {
      if (errConnect) {
        return reject(errConnect);
      }

      var aggregate = args.aggregate ? args.aggregate : {};

      if (args.numPage || args.size) {
        var pagination = getPagination(args.numPage, args.size);
        args.offset = pagination.offset;
        args.limit = pagination.limit;
      } // rajout de la pagination dans l'aggregate :


      if (args.offset || args.limit) {
        aggregate.push({
          $facet: {
            total: [{
              $count: 'count'
            }],
            data: [{
              $addFields: {
                _id: '$_id'
              }
            }]
          }
        }, {
          $unwind: '$total'
        }, {
          $project: {
            items: {
              $slice: ['$data', args.offset, {
                $ifNull: [args.limit, '$total.count']
              }]
            },
            page: {
              $literal: args.offset / args.limit + 1
            },
            totalPages: {
              $ceil: {
                $divide: ['$total.count', args.limit]
              }
            },
            totalItems: '$total.count'
          }
        });
      }

      return client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME).collection(args.type).aggregate(aggregate, {
        allowDiskUse: true
      }).toArray(function (error, res) {
        if (error) {
          return reject(error);
        }

        client.close();

        if (args.offset || args.limit) {
          var _res$, _res$2, _res$3;

          resolve({
            nbCount: ((_res$ = res[0]) === null || _res$ === void 0 ? void 0 : _res$.totalItems) || 0,
            nbPages: ((_res$2 = res[0]) === null || _res$2 === void 0 ? void 0 : _res$2.totalPages) || 0,
            res: ((_res$3 = res[0]) === null || _res$3 === void 0 ? void 0 : _res$3.items) || []
          });
        } else {
          return resolve(res);
        }
      });
    });
  });
}
/*
 * Global Data Access Helpers for INSERT
return add({
  type: TABLES.XXX,
  data: data,
});
 */

function add(args) {
  return new Promise(function (resolve, reject) {
    return mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(url, {
      useNewUrlParser: true
    }, function (errConnect, client) {
      if (errConnect) {
        return reject(errConnect);
      } // Get new id value for table


      return incrementCounter(args.type, client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME), function (err, newId) {
        // do INSERT
        if (_configs_DataConfig__WEBPACK_IMPORTED_MODULE_3__.HAS_AUTOINCREMENT[args.type]) {
          args.data[newId.value.name] = newId.value.seq;
        }

        args.data = _objectSpread(_objectSpread({}, args.data), {}, {
          created_at: new Date(),
          updated_at: new Date()
        });
        client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME).collection(args.type).insertOne(args.data, function (error, res) {
          if (error) {
            return reject(error);
          }

          client.close();

          if (res.result.ok === 1) {
            return resolve({
              affectedRows: res.insertedCount,
              insertId: res.ops[0][newId.value.name],
              result: res
            });
          }

          return reject(Error({
            affectedRows: 0,
            insertId: undefined,
            result: res
          }));
        });
      });
    });
  });
}
/*
 * Global Data Access Helpers for INSERT Multiple
return addMult({
  type: TABLES.XXX,
  keys: [keys],
  data: [[val], [val]],
});
 */

function addMult(args) {
  var connection = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return new Promise(function (resolve, reject) {
    return mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(url, {
      useNewUrlParser: true
    }, function (errConnect, client) {
      if (errConnect) {
        return reject(errConnect);
      }

      return incrementCounterMult(args.type, client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME), (0,lodash__WEBPACK_IMPORTED_MODULE_1__.values)(args.data).length + 1, function (err, newId) {
        // do INSERT
        var incId = parseInt(newId.value.seq, 10);
        var ret = (0,lodash__WEBPACK_IMPORTED_MODULE_1__.map)(args.data, function (arg, index) {
          if (_configs_DataConfig__WEBPACK_IMPORTED_MODULE_3__.HAS_AUTOINCREMENT[args.type]) {
            arg[newId.value.name] = incId + (parseInt(index, 10) + 1);
          }

          return _objectSpread(_objectSpread({}, arg), {}, {
            created_at: new Date(),
            updated_at: new Date()
          });
        }); // we need to add data in the bdd

        return client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME).collection(args.type).insertMany(ret, function (error, res) {
          if (error) {
            return reject(error);
          }

          client.close();
          return resolve(res);
        });
      });
    });
  });
}
/*
 * Global Data Access Helpers for UPDATE
return update({
  type: TABLES.XXX,
  data: data,
  where: { property: [whereArgs] },
});
 */

function update(args) {
  var connection = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return new Promise(function (resolve, reject) {
    return mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(url, {
      useNewUrlParser: true
    }, function (errConnect, client) {
      if (errConnect) {
        return reject(errConnect);
      }

      args.data = _objectSpread(_objectSpread({}, args.data), {}, {
        updated_at: new Date()
      });
      return client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME).collection(args.type).updateMany(args.where, _defineProperty({}, args.action || '$set', args.data), function (error, res) {
        if (error) {
          return reject(error);
        }

        client.close();
        return resolve({
          affectedRows: res.result.nModified
        });
      });
    });
  });
}
/*
 * Global Data Access Helpers for DELETE
return remove({
  type: TABLES.XXX,
  where: { property: [whereArgs] },
});
 */

function remove(args) {
  var connection = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  return new Promise(function (resolve, reject) {
    return mongodb__WEBPACK_IMPORTED_MODULE_0__.MongoClient.connect(url, {
      useNewUrlParser: true
    }, function (errConnect, client) {
      if (errConnect) {
        return reject(errConnect);
      }

      return client.db(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.DB.DB_NAME).collection(args.type).deleteMany(args.where, function (error, res) {
        if (error) {
          return reject(error);
        }

        client.close();
        return resolve({
          affectedRows: res.deletedCount
        });
      });
    });
  });
}

/***/ }),

/***/ "./app/utils/ErrorUtils.js":
/*!*********************************!*\
  !*** ./app/utils/ErrorUtils.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ERROR_API": () => (/* binding */ ERROR_API),
/* harmony export */   "ERROR_DB": () => (/* binding */ ERROR_DB),
/* harmony export */   "sendErrorResponse": () => (/* binding */ sendErrorResponse)
/* harmony export */ });
/*****************************************************************************
 * Error Utils
 *****************************************************************************/

/*
 * API Server Errors Code
 */
var ERROR_API = {
  INTERNAL: 'INTERNAL_ERROR',
  DATA_BAD_BODY: 'ER_DATA_BAD_BODY',
  REQUEST_BAD_HEADERS: 'ER_REQUEST_BAD_HEADERS',
  BAD_AUTHORIZATION: 'BAD_AUTHORIZATION',
  FORBIDDEN_ACCESS: 'FORBIDDEN_ACCESS',
  BAD_AUTHORIZATION_GOOGLE_API: 'BAD_AUTHORIZATION_GOOGLE_API'
};
/*
 * Database Errors Code
 */

var ERROR_DB = {
  // CONNECTION: 'ER_ACCESS_DENIED_ERROR',
  DUPLICATE: 'ER_DUP_ENTRY'
};
/*
 * API Server Error Response
 */

function sendErrorResponse() {
  var err = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var res = arguments.length > 1 ? arguments[1] : undefined;
  var msg = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'Error';

  if (err.message) {
    console.error(err.message);
  }

  if (!err.hideStack && err.stack) {
    console.error(err.stack);
  }

  var errorMessage = '';

  if (err.statusText) {
    errorMessage = "".concat(msg, ": ").concat(err.statusText);
  } else if (typeof err === 'string') {
    errorMessage = "".concat(msg, ": ").concat(err);
  } else {
    errorMessage = msg;
  }

  console.error(errorMessage);
  return res.status(err.statusCode || 500).send({
    status: 'error',
    code: err.code || ERROR_API.INTERNAL,
    data: err.data,
    message: errorMessage
  });
}

/***/ }),

/***/ "./app/utils/HashGenerator.js":
/*!************************************!*\
  !*** ./app/utils/HashGenerator.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "generateHash": () => (/* binding */ generateHash),
/* harmony export */   "getModelHash": () => (/* binding */ getModelHash)
/* harmony export */ });
/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! crypto */ "crypto");
/* harmony import */ var crypto__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(crypto__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_sha512__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! js-sha512 */ "js-sha512");
/* harmony import */ var js_sha512__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_sha512__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*****************************************************************************
 * Hash Generator
 *****************************************************************************/


/*
 * Generate a Hash
 */

function generateHash() {
  var hashLength = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 128;
  var errorMessage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'Cannot generate a Hash';
  return new Promise(function (resolve, reject) {
    crypto__WEBPACK_IMPORTED_MODULE_0___default().randomBytes(hashLength, function (err, buf) {
      if (err) {
        return reject(Error(_objectSpread(_objectSpread({}, err), {}, {
          statusText: errorMessage
        })));
      }

      return resolve(buf.toString('hex'));
    });
  });
}
/*
 * Generate the Model access token
 */

function getModelHash(data) {
  return js_sha512__WEBPACK_IMPORTED_MODULE_1___default().sha512("\n    ".concat(data.model_id, ";\n  ")).substring(0, 8);
}

/***/ }),

/***/ "./app/utils/Mailer.js":
/*!*****************************!*\
  !*** ./app/utils/Mailer.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "sendTemplateMail": () => (/* binding */ sendTemplateMail),
/* harmony export */   "sendTemplateMails": () => (/* binding */ sendTemplateMails)
/* harmony export */ });
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../configs/ServerConfig */ "./app/configs/ServerConfig.js");
/* harmony import */ var _configs_Constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../configs/Constants */ "./app/configs/Constants.js");
/* harmony import */ var _templates_mails__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templates/mails */ "./app/templates/mails/index.js");
function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == _typeof(value) && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator["return"] && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) { if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; } return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) { keys.push(key); } return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) { "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); } }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, "catch": function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/*****************************************************************************
 * Mailer Utils
 *****************************************************************************/
// Récupération de Blue Live






var SparkPost = __webpack_require__(/*! sparkpost */ "sparkpost");

var nodemailer = __webpack_require__(/*! nodemailer */ "nodemailer");

var options = {}; // en production l'api est sur le domaine api.eu.sparkpost.com
// en recette ou dev c'est la valeur par défault api.sparkpost.com
// test
//  if (process.env.NODE_ENV === 'production') options = { origin: 'https://api.eu.sparkpost.com:443' };

var mailjet = {};
var client = {};

if (_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.IS_MAILJET && _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.MJ_APIKEY_PUBLIC && _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.MJ_APIKEY_PRIVATE) {
  mailjet = (__webpack_require__(/*! node-mailjet */ "node-mailjet").apiConnect)(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.MJ_APIKEY_PUBLIC, _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.MJ_APIKEY_PRIVATE);
}

if (_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.SPARKPOST_API_KEY) {
  client = new SparkPost(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.SPARKPOST_API_KEY, options);
}

var transporter = nodemailer.createTransport({
  host: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.SMTP_HOST,
  port: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.SMTP_PORT,
  auth: {
    user: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.SMTP_USER,
    password: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.SMTP_PASSWORD
  }
});
/*
 * Send a mail returning a promise
 */

function sendMail(parameters) {
  var destinataires = Array.isArray(parameters.to) ? parameters.to : [parameters.to];
  var recipientsMap = (0,lodash__WEBPACK_IMPORTED_MODULE_1__.map)(destinataires, function (dest) {
    return {
      address: {
        email: dest,
        name: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.split)(dest, '@')[0]
      }
    };
  });
  return new Promise( /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(resolve, reject) {
      var ccParams;
      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              ccParams = [];

              if (!_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.IS_MAILJET) {
                _context.next = 4;
                break;
              }

              if (parameters.cc !== true && !(0,lodash__WEBPACK_IMPORTED_MODULE_1__.isEmpty)(parameters.cc)) {
                ccParams = [{
                  Email: parameters.cc,
                  Name: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.split)(parameters.cc, '@')[0]
                }];
              }

              return _context.abrupt("return", mailjet.post('send', {
                version: 'v3.1'
              }).request(ccParams ? {
                Messages: [{
                  From: {
                    Email: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.FROM,
                    Name: 'Anaëlle CLEMENT'
                  },
                  To: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.map)(destinataires, function (dest) {
                    return {
                      Email: dest,
                      Name: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.split)(dest, '@')[0]
                    };
                  }),
                  // TextPart: 'Greetings from Mailjet!',
                  Subject: parameters.subject,
                  Cc: ccParams,
                  HTMLPart: parameters.html,
                  TextPart: parameters.text
                }]
              } : {
                Messages: [{
                  From: {
                    Email: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.FROM,
                    Name: 'Anaëlle CLEMENT'
                  },
                  To: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.map)(destinataires, function (dest) {
                    return {
                      Email: dest,
                      Name: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.split)(dest, '@')[0]
                    };
                  }),
                  // TextPart: 'Greetings from Mailjet!',
                  Subject: parameters.subject,
                  HTMLPart: parameters.html,
                  TextPart: parameters.text
                }]
              }).then(function (result) {
                resolve(result && result.body);
              })["catch"](function (err) {
                console.error('ERROR : Something went wrong with mailjet', err);
              }));

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }());
}
/*
 * Send (multiple) mails with a template
 */


function sendTemplateMails(templateProp, params, to, cc, withAttachment) {
  var _TEMPLATES_MAIL_PROPS = _templates_mails__WEBPACK_IMPORTED_MODULE_4__.TEMPLATES_MAIL_PROPS[templateProp],
      templateHtml = _TEMPLATES_MAIL_PROPS.templateHtml,
      templateTxt = _TEMPLATES_MAIL_PROPS.templateTxt,
      subject = _TEMPLATES_MAIL_PROPS.subject;
  var content = [];
  return new Promise(function (resolve, reject) {
    // récupération de blue live
    // if (withAttachment) {
    //   const path = __dirname + '/../app/static/mails/bluelive.pdf';
    //   const fileMail = fs.createReadStream(path);
    //   fileMail.on('data', f => {
    //     content.push(f);
    //   });
    //   fileMail.on('end', f => {
    //     console.log('end file');
    //     resolve(content);
    //   });
    // } else {
    resolve(); // }
  }).then(function (fileContent) {
    var mailBody = {}; // récupération de blue live
    // if (withAttachment) {
    //   mailBody = {
    //     attachments: [
    //       {
    //         type: 'application/pdf',
    //         name: 'bluelive.pdf',
    //         data: new Buffer.concat(fileContent).toString('base64')
    //       }
    //     ]
    //   };
    // }

    mailBody = _objectSpread(_objectSpread({}, mailBody), {}, {
      text: templateTxt(_objectSpread({
        APP_URL: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.APP_URL,
        APP_URL_FRONT: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.APP_URL_FRONT,
        CONTACT_EMAIL: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.CONTACT_EMAIL,
        APP_URL_STATIC: _configs_Constants__WEBPACK_IMPORTED_MODULE_3__.APP_URL_STATIC,
        APP_WEBCAL_STATIC: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.APP_WEBCAL_STATIC
      }, params)),
      html: templateHtml(_objectSpread({
        APP_URL: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.APP_URL,
        APP_URL_FRONT: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.APP_URL_FRONT,
        CONTACT_EMAIL: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.CONTACT_EMAIL,
        APP_URL_STATIC: _configs_Constants__WEBPACK_IMPORTED_MODULE_3__.APP_URL_STATIC,
        APP_WEBCAL_STATIC: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.APP_WEBCAL_STATIC
      }, params))
    });
    return sendMail(_objectSpread({
      from: _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.MAIL.FROM,
      to: to,
      cc: cc,
      // subject: MAIL.SUBJECT_PREFIX
      //   ? `${MAIL.SUBJECT_PREFIX} - ${subject(params)}`
      //   : subject(params),
      subject: subject(params)
    }, mailBody));
  });
}
/*
 * Send a mail with a template
 */

function sendTemplateMail(templateProp, params, to, cc) {
  var withAttachment = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  return sendTemplateMails(templateProp, params, to, cc, withAttachment).then(function (mailResponse) {
    if (!mailResponse ||  true && _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.IS_MAILJET !== 1 && mailResponse.results.total_accepted_recipients === 0 || _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_2__.SERVER.IS_MAILJET && mailResponse.Messages[0].Status !== 'success') {
      console.error('ERROR mailResponse', JSON.stringify(mailResponse));
      throw String('Unable to send the Email');
    } else {
      console.log('Mail envoyé');
    }
  })["catch"](function (err) {
    console.error('ERROR sendTemplateMail', err);
    throw String('Unable to send the Email');
  });
}

/***/ }),

/***/ "./app/utils/PropsTypes.js":
/*!*********************************!*\
  !*** ./app/utils/PropsTypes.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/*****************************************************************************
 * Props Validator
 *****************************************************************************/
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  string: '[object String]',
  email: '[object Email]',
  datetime: '[object Date]',
  "boolean": '[object Boolean]',
  number: '[object Number]',
  objectJSON: '[object Object]',
  array: '[object Array]',
  arrayNotEmpty: '[object ArrayNonEmpty]',
  "null": '[object Null]',
  undefined: '[object Undefined]'
});

/***/ }),

/***/ "./app/utils/RequestsUtils.js":
/*!************************************!*\
  !*** ./app/utils/RequestsUtils.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "checkAdminRole": () => (/* binding */ checkAdminRole),
/* harmony export */   "checkAuthentication": () => (/* binding */ checkAuthentication),
/* harmony export */   "checkDoSomethingtRole": () => (/* binding */ checkDoSomethingtRole),
/* harmony export */   "checkHeadersAcceptJSONResponse": () => (/* binding */ checkHeadersAcceptJSONResponse),
/* harmony export */   "checkLocalhostRole": () => (/* binding */ checkLocalhostRole),
/* harmony export */   "checkUserRole": () => (/* binding */ checkUserRole)
/* harmony export */ });
/* harmony import */ var js_sha512__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! js-sha512 */ "js-sha512");
/* harmony import */ var js_sha512__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(js_sha512__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lodash */ "lodash");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _DAO__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DAO */ "./app/utils/DAO.js");
/* harmony import */ var _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ErrorUtils */ "./app/utils/ErrorUtils.js");
/* harmony import */ var _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../configs/ServerConfig */ "./app/configs/ServerConfig.js");
/* harmony import */ var _configs_DataConfig__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../configs/DataConfig */ "./app/configs/DataConfig.js");
/* harmony import */ var _RolesUtils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./RolesUtils */ "./app/utils/RolesUtils.js");
/*****************************************************************************
 * Request Utils Controller
 *****************************************************************************/







/*
 * Check that Headers Accept JSON responses
 */

function checkHeadersAcceptJSONResponse(req, res, next) {
  if (!req.accepts('application/json')) {
    return (0,_ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.sendErrorResponse)({
      statusCode: 406,
      code: _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.ERROR_API.REQUEST_BAD_HEADERS
    }, res, "Headers error: Doesn't Accept JSON responses");
  }

  res.contentType('application/json');
  return next();
}
/*
 * Check Authentication in headers
 */

function checkAuthentication(req, res, next) {
  console.log('Check Authentication in Headers');
  var accountid = req.headers.accountid || req.query.accountid;
  var authorization = req.headers.authorization || req.query.authorization;
  return Promise.resolve().then(function () {
    if (!accountid || !authorization || authorization.length === 0) {
      console.error("Headers error: userid=".concat(accountid, " / token=").concat(authorization));
      throw Object({
        statusCode: 401,
        code: _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.ERROR_API.BAD_AUTHORIZATION,
        statusText: 'Request without userid or authorization'
      });
    }

    return (0,_DAO__WEBPACK_IMPORTED_MODULE_2__.get)({
      type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_5__.TABLES.USER_CONNECTIONS,
      where: {
        account_id: parseInt(accountid, 0)
      },
      orderBy: {
        created_at: -1
      },
      limit: 5
    });
  }).then(function (rows) {
    var matchedRow = (0,lodash__WEBPACK_IMPORTED_MODULE_1__.find)(rows, function (row) {
      return row && row.token && row.token === js_sha512__WEBPACK_IMPORTED_MODULE_0___default().sha512(authorization);
    });

    if (matchedRow) {
      console.log("User connected with ID: ".concat(accountid));
      return (0,_DAO__WEBPACK_IMPORTED_MODULE_2__.get)({
        type: _configs_DataConfig__WEBPACK_IMPORTED_MODULE_5__.TABLES.ACCOUNTS,
        select: {
          account_id: true,
          roles: true
        },
        where: {
          account_id: parseInt(accountid, 0)
        }
      });
    }

    throw Object({
      statusCode: 401,
      code: _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.ERROR_API.BAD_AUTHORIZATION,
      statusText: 'Request with bad userid or authorization'
    });
  }).then(function (rows) {
    req.account = rows[0];
    return next();
  })["catch"](function (err) {
    return (0,_ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.sendErrorResponse)(err, res, 'An error occurred when checking the authorization token');
  });
}
/*
 * Check that user roles is ADMIN
 */

function checkAdminRole(req, res, next) {
  if (!req.account || !(0,_RolesUtils__WEBPACK_IMPORTED_MODULE_6__.isAdmin)(req.account)) {
    return (0,_ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.sendErrorResponse)({
      statusCode: 403,
      code: _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.ERROR_API.FORBIDDEN_ACCESS,
      statusText: 'This route require an Administrator role.'
    }, res, 'An error occurred when checking the authorization role');
  }

  return next();
}
/*
 * Check that user roles is USER
 */

function checkUserRole(req, res, next) {
  if (!req.account || !(0,_RolesUtils__WEBPACK_IMPORTED_MODULE_6__.isUser)(req.account)) {
    return (0,_ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.sendErrorResponse)({
      statusCode: 403,
      code: _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.ERROR_API.FORBIDDEN_ACCESS,
      statusText: 'This route requires an User role.'
    }, res, 'An error occurred when checking the authorization role');
  }

  return next();
}
/*
 * Check that user roles can Do Something [Example]
 */

function checkDoSomethingtRole(req, res, next) {
  if (!req.account || !(0,_RolesUtils__WEBPACK_IMPORTED_MODULE_6__.canDoSomething)(req.account)) {
    return (0,_ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.sendErrorResponse)({
      statusCode: 403,
      code: _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.ERROR_API.FORBIDDEN_ACCESS,
      statusText: 'This route require a Do Something role.'
    }, res, 'An error occurred when checking the authorization role');
  }

  return next();
}
/*
 * Check that request is send from Localhost (for Batches)
 */

function checkLocalhostRole(req, res, next) {
  var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

  if (ip !== '::1' && ip !== '127.0.0.1' && ip !== '::ffff:127.0.0.1' || req.headers.accountid !== req.headers.authorization || req.headers.accountid !== _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_4__.SERVER.LOCALHOST_TOKEN) {
    return (0,_ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.sendErrorResponse)({
      statusCode: 403,
      code: _ErrorUtils__WEBPACK_IMPORTED_MODULE_3__.ERROR_API.FORBIDDEN_ACCESS,
      statusText: 'Cette route requiert le rôle Localhost.'
    }, res, "Une erreur s'est produite lors de la verification de votre rôle");
  }

  return next();
}

/***/ }),

/***/ "./app/utils/RolesUtils.js":
/*!*********************************!*\
  !*** ./app/utils/RolesUtils.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "canDoSomething": () => (/* binding */ canDoSomething),
/* harmony export */   "isAdmin": () => (/* binding */ isAdmin),
/* harmony export */   "isUser": () => (/* binding */ isUser)
/* harmony export */ });
/* harmony import */ var _configs_Properties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../configs/Properties */ "./app/configs/Properties.js");

function isAdmin(userAccount) {
  var uniqRole = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  return (!uniqRole || userAccount.roles.length === 1) && userAccount.roles.indexOf(_configs_Properties__WEBPACK_IMPORTED_MODULE_0__.ACCOUNTS_ROLES.ADMIN) !== -1;
}
function isUser(userAccount) {
  var uniqRole = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  return (!uniqRole || userAccount.roles.length === 1) && userAccount.roles.indexOf(_configs_Properties__WEBPACK_IMPORTED_MODULE_0__.ACCOUNTS_ROLES.USER || _configs_Properties__WEBPACK_IMPORTED_MODULE_0__.ACCOUNTS_ROLES.USER_SECONDARY) !== -1;
}
function canDoSomething(userAccount) {
  return isAdmin(userAccount) || isUser(userAccount);
}

/***/ }),

/***/ "./app/utils/StringUtils.js":
/*!**********************************!*\
  !*** ./app/utils/StringUtils.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getExtension": () => (/* binding */ getExtension),
/* harmony export */   "pad": () => (/* binding */ pad),
/* harmony export */   "removeExtension": () => (/* binding */ removeExtension),
/* harmony export */   "upperFirst": () => (/* binding */ upperFirst)
/* harmony export */ });
function upperFirst(string) {
  return string[0].toUpperCase() + string.substr(1);
}
function pad(source, length) {
  var _char = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '0';

  var string = source.toString();
  return string.length < length ? pad(_char + string, length, _char) : string;
}
function getExtension(filename) {
  var i = filename.lastIndexOf('.');
  return i < 0 ? '' : filename.substr(i);
}
function removeExtension(filename) {
  var lastDotPosition = filename.lastIndexOf('.');
  if (lastDotPosition === -1) return filename;else return filename.substr(0, lastDotPosition);
}

/***/ }),

/***/ "./app/utils/loggerFormat.js":
/*!***********************************!*\
  !*** ./app/utils/loggerFormat.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (function (tokens, req, res) {
  var status = tokens.status(req, res); // get status color

  var color = 0;
  if (status >= 500) color = 31; // red

  if (status >= 400) color = 33; // yellow

  if (status >= 300) color = 36; // cyan

  if (status >= 200) color = 32; // green

  return ["\x1B[0m".concat(new Date().toISOString()), tokens.method(req, res), tokens.url(req, res), "\x1B[".concat(color, "m").concat(tokens.status(req, res)), "\x1B[0m".concat(tokens['response-time'](req, res), " ms"), '-', tokens.res(req, res, 'content-length')].join(' ');
});

/***/ }),

/***/ "./app/utils/validateData.js":
/*!***********************************!*\
  !*** ./app/utils/validateData.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "validateData": () => (/* binding */ validateData)
/* harmony export */ });
var options = {
  abortEarly: false,
  // include all errors
  allowUnknown: true,
  // ignore unknown props
  stripUnknown: true // remove unknown props

};
function validateData(schema, data) {
  var _schema$validate = schema.validate(data, options),
      error = _schema$validate.error,
      value = _schema$validate.value;

  if (error) {
    // on fail return comma separated errors
    throw String("Erreur de validation des donn\xE9es: ".concat(error.details.map(function (x) {
      return x.message;
    }).join(', ')));
  } else {
    return true;
  }
}

/***/ }),

/***/ "./app/templates/mails/account-creation-membre.html":
/*!**********************************************************!*\
  !*** ./app/templates/mails/account-creation-membre.html ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<!DOCTYPE html>\r\n<html lang="en">\r\n<head>\r\n    <meta charset="UTF-8">\r\n    <title>Title</title>\r\n</head>\r\n<body>\r\n\r\n</body>\r\n</html>';

}
return __p
}

/***/ }),

/***/ "./app/templates/mails/account-creation-membre.txt":
/*!*********************************************************!*\
  !*** ./app/templates/mails/account-creation-membre.txt ***!
  \*********************************************************/
/***/ ((module) => {

module.exports = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '';

}
return __p
}

/***/ }),

/***/ "./app/templates/mails/account-new-password.html":
/*!*******************************************************!*\
  !*** ./app/templates/mails/account-new-password.html ***!
  \*******************************************************/
/***/ ((module) => {

module.exports = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<!doctype html>\r\n<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"\r\n      xmlns:o="urn:schemas-microsoft-com:office:office">\r\n\r\n<head>\r\n    <title>\r\n    </title>\r\n    <!--[if !mso]><!-->\r\n    <meta http-equiv="X-UA-Compatible" content="IE=edge">\r\n    <!--<![endif]-->\r\n    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\r\n    <meta name="viewport" content="width=device-width, initial-scale=1">\r\n    <style type="text/css">\r\n        #outlook a {\r\n            padding: 0;\r\n        }\r\n\r\n        body {\r\n            margin: 0;\r\n            padding: 0;\r\n            -webkit-text-size-adjust: 100%;\r\n            -ms-text-size-adjust: 100%;\r\n        }\r\n\r\n        table,\r\n        td {\r\n            border-collapse: collapse;\r\n            mso-table-lspace: 0pt;\r\n            mso-table-rspace: 0pt;\r\n        }\r\n\r\n        img {\r\n            border: 0;\r\n            height: auto;\r\n            line-height: 100%;\r\n            outline: none;\r\n            text-decoration: none;\r\n            -ms-interpolation-mode: bicubic;\r\n        }\r\n\r\n        p {\r\n            display: block;\r\n            margin: 13px 0;\r\n        }\r\n\r\n    </style>\r\n    <!--[if mso]>\r\n    <noscript>\r\n        <xml>\r\n            <o:OfficeDocumentSettings>\r\n                <o:AllowPNG/>\r\n                <o:PixelsPerInch>96</o:PixelsPerInch>\r\n            </o:OfficeDocumentSettings>\r\n        </xml>\r\n    </noscript>\r\n    <![endif]-->\r\n    <!--[if lte mso 11]>\r\n    <style type="text/css">\r\n        .mj-outlook-group-fix {\r\n            width: 100% !important;\r\n        }\r\n    </style>\r\n    <![endif]-->\r\n    <style type="text/css">\r\n        @media only screen and (min-width: 480px) {\r\n            .mj-column-per-50 {\r\n                width: 50% !important;\r\n                max-width: 50%;\r\n            }\r\n\r\n            .mj-column-per-100 {\r\n                width: 100% !important;\r\n                max-width: 100%;\r\n            }\r\n        }\r\n\r\n    </style>\r\n    <style media="screen and (min-width:480px)">\r\n        .moz-text-html .mj-column-per-50 {\r\n            width: 50% !important;\r\n            max-width: 50%;\r\n        }\r\n\r\n        .moz-text-html .mj-column-per-100 {\r\n            width: 100% !important;\r\n            max-width: 100%;\r\n        }\r\n\r\n    </style>\r\n    <style type="text/css">\r\n        @media only screen and (max-width: 480px) {\r\n            table.mj-full-width-mobile {\r\n                width: 100% !important;\r\n            }\r\n\r\n            td.mj-full-width-mobile {\r\n                width: auto !important;\r\n            }\r\n        }\r\n\r\n    </style>\r\n    <style type="text/css">\r\n    </style>\r\n</head>\r\n\r\n<body style="word-spacing:normal;background-color:#f2f2f2;">\r\n<div style="background-color:#f2f2f2;">\r\n    <!--[if mso | IE]>\r\n    <table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;"\r\n           width="600">\r\n        <tr>\r\n            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n    <div style="margin:0px auto;max-width:600px;">\r\n        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">\r\n            <tbody>\r\n            <tr>\r\n                <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:20px;padding-top:20px;text-align:center;">\r\n                    <!--[if mso | IE]>\r\n                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">\r\n                        <tr>\r\n                            <td class="" style="vertical-align:middle;width:300px;"><![endif]-->\r\n                    <div class="mj-column-per-50 mj-outlook-group-fix"\r\n                         style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">\r\n                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"\r\n                               style="vertical-align:middle;" width="100%">\r\n                            <tbody>\r\n                            <tr>\r\n                                <td align="left"\r\n                                    style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:25px;padding-bottom:0px;padding-left:25px;word-break:break-word;">\r\n                                    <div style="font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:11px;line-height:1;text-align:left;color:#000000;">\r\n                                        <span style="font-size: 15px">Choisissez un nouveau mot de passe</span></div>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                    <!--[if mso | IE]></td></tr></table><![endif]-->\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n    <!--[if mso | IE]></td></tr></table>\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;"\r\n       width="600" bgcolor="#ffffff">\r\n    <tr>\r\n        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">\r\n            <v:rect style="width:600px;" xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false">\r\n                <v:fill origin="0.5, 0" position="0.5, 0" src="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg"\r\n                        color="#ffffff" type="tile" size="1,1" aspect="atleast"/>\r\n                <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0"><![endif]-->\r\n    <div style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg\') center top / cover repeat;background-position:center top;background-repeat:repeat;background-size:cover;margin:0px auto;max-width:600px;">\r\n        <div style="line-height:0;font-size:0;">\r\n            <table align="center" background="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg" border="0" cellpadding="0"\r\n                   cellspacing="0" role="presentation"\r\n                   style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg\') center top / cover repeat;background-position:center top;background-repeat:repeat;background-size:cover;width:100%;">\r\n                <tbody>\r\n                <tr>\r\n                    <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0;text-align:center;">\r\n                        <!--[if mso | IE]>\r\n                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">\r\n                            <tr>\r\n                                <td class="" style="vertical-align:top;width:600px;"><![endif]-->\r\n                        <div class="mj-column-per-100 mj-outlook-group-fix"\r\n                             style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\r\n                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"\r\n                                   style="vertical-align:top;" width="100%">\r\n                                <tbody>\r\n                                <tr>\r\n                                    <td align="left"\r\n                                        style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;">\r\n                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"\r\n                                               style="border-collapse:collapse;border-spacing:0px;">\r\n                                            <tbody>\r\n                                            <tr>\r\n                                                <td style="width:231px;">\r\n                                                    <a href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'" target="_blank">\r\n                                                        <img alt="FFB logo" height="auto"\r\n                                                             src="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/logo.png"\r\n                                                             style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"\r\n                                                             width="231"/>\r\n                                                    </a>\r\n                                                </td>\r\n                                            </tr>\r\n                                            </tbody>\r\n                                        </table>\r\n                                    </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                        <!--[if mso | IE]></td></tr></table><![endif]-->\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n    <!--[if mso | IE]></v:textbox></v:rect></td></tr></table>\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;"\r\n       width="600" bgcolor="#ffffff">\r\n    <tr>\r\n        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">\r\n            <v:rect style="width:600px;" xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false">\r\n                <v:fill origin="0, -0.5" position="0, -0.5" src="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg"\r\n                        color="#ffffff" type="frame" size="100%" aspect="atmost"/>\r\n                <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0"><![endif]-->\r\n    <div style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg\') center top / 100% no-repeat;background-position:center top;background-repeat:no-repeat;background-size:100%;margin:0px auto;max-width:600px;">\r\n        <div style="line-height:0;font-size:0;">\r\n            <table align="center" background="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg" border="0" cellpadding="0"\r\n                   cellspacing="0" role="presentation"\r\n                   style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg\') center top / 100% no-repeat;background-position:center top;background-repeat:no-repeat;background-size:100%;width:100%;">\r\n                <tbody>\r\n                <tr>\r\n                    <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:40px;padding-top:20px;text-align:center;">\r\n                        <!--[if mso | IE]>\r\n                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">\r\n                            <tr>\r\n                                <td class="" style="vertical-align:top;width:600px;"><![endif]-->\r\n                        <div class="mj-column-per-100 mj-outlook-group-fix"\r\n                             style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\r\n                            <table border="0" cellpadding="0" cellspacing="0" role="presentation"\r\n                                   style="vertical-align:top;" width="100%">\r\n                                <tbody>\r\n                                <tr>\r\n                                    <td align="justify" style="font-size:0px;padding:10px 25px;word-break:break-word;">\r\n                                        <div style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:justify;color:#000000;">\r\n                                            Bonjour <strong>' +
((__t = ( user.pseudo )) == null ? '' : __t) +
'</strong>,\r\n                                        </div>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td align="justify" style="font-size:0px;padding:10px 25px;word-break:break-word;">\r\n                                        <div style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:justify;color:#000000;">\r\n                                            Vous avez perdu votre mot de passe.\r\n                                        </div>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td\r\n                                            align="center"\r\n                                            vertical-align="middle"\r\n                                            style="font-size:0px;padding:15px 30px;padding-top:10px;padding-right:25px;padding-bottom:10px;padding-left:25px;word-break:break-word;"\r\n                                    >\r\n                                        <table\r\n                                                border="0"\r\n                                                cellpadding="0"\r\n                                                cellspacing="0"\r\n                                                role="presentation"\r\n                                                style="border-collapse:separate;line-height:100%;"\r\n                                        >\r\n                                            <tr>\r\n                                                <td\r\n                                                        align="center"\r\n                                                        bgcolor="#1a7ba6"\r\n                                                        role="presentation"\r\n                                                        style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#1a7ba6;"\r\n                                                        valign="middle"\r\n                                                >\r\n                                                    <a\r\n                                                            href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'/definir-mot-de-passe/' +
((__t = ( user.email )) == null ? '' : __t) +
'/' +
((__t = ( user.utilisateur_id )) == null ? '' : __t) +
'/' +
((__t = ( user.token )) == null ? '' : __t) +
'"\r\n                                                            style="display:inline-block;background:#1a7ba6;color:#FFFFFF;font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"\r\n                                                            target="_blank"\r\n                                                    >\r\n                                                        Cliquez ici pour en choisir un nouveau\r\n                                                    </a>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </table>\r\n                                    </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                        <!--[if mso | IE]></td>\r\n                    <td class="" style="vertical-align:top;width:60px;"><![endif]-->\r\n                        <div\r\n                                class="mj-column-per-10 mj-outlook-group-fix"\r\n                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                        >\r\n                            <table\r\n                                    border="0"\r\n                                    cellpadding="0"\r\n                                    cellspacing="0"\r\n                                    role="presentation"\r\n                                    style="vertical-align:top;"\r\n                                    width="100%"\r\n                            >\r\n                                <tbody></tbody>\r\n                            </table>\r\n                        </div>\r\n                        <!--[if mso | IE]></td></tr></table><![endif]-->\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n        <!--[if mso | IE]></td></tr></table>\r\n    <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600">\r\n        <tr>\r\n            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n        <div style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;max-width:600px;">\r\n            <table\r\n                    align="center"\r\n                    border="0"\r\n                    cellpadding="0"\r\n                    cellspacing="0"\r\n                    role="presentation"\r\n                    style="background:#f2f2f2;background-color:#f2f2f2;width:100%;"\r\n            >\r\n                <tbody>\r\n                <tr>\r\n                    <td\r\n                            style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:30px;text-align:center;"\r\n                    >\r\n                        <!--[if mso | IE]>\r\n                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">\r\n                            <tr>\r\n                                <td class="" style="vertical-align:top;width:170px;"><![endif]-->\r\n                        <div\r\n                                class="mj-column-px-170 mj-outlook-group-fix"\r\n                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                        >\r\n                            <table\r\n                                    border="0"\r\n                                    cellpadding="0"\r\n                                    cellspacing="0"\r\n                                    role="presentation"\r\n                                    style="vertical-align:top;"\r\n                                    width="100%"\r\n                            >\r\n                                <tbody>\r\n                                <tr>\r\n                                    <td\r\n                                            align="center"\r\n                                            style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;"\r\n                                    >\r\n                                        <table\r\n                                                border="0"\r\n                                                cellpadding="0"\r\n                                                cellspacing="0"\r\n                                                role="presentation"\r\n                                                style="border-collapse:collapse;border-spacing:0px;"\r\n                                        >\r\n                                            <td\r\n                                                    align="center"\r\n                                                    vertical-align="middle"\r\n                                                    style="font-size:0px;padding:15px 30px;padding-top:10px;padding-right:25px;padding-bottom:10px;padding-left:25px;word-break:break-word;"\r\n                                            >\r\n                                                <table\r\n                                                        border="0"\r\n                                                        cellpadding="0"\r\n                                                        cellspacing="0"\r\n                                                        role="presentation"\r\n                                                        style="border-collapse:separate;line-height:100%;"\r\n                                                >\r\n                                                    <tr>\r\n                                                        <td\r\n                                                                align="center"\r\n                                                                bgcolor="#1a7ba6"\r\n                                                                role="presentation"\r\n                                                                style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#1a7ba6;"\r\n                                                                valign="middle"\r\n                                                        >\r\n                                                            <a\r\n                                                                    href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'/definir-mot-de-passe/' +
((__t = ( user.email )) == null ? '' : __t) +
'/' +
((__t = ( user.utilisateur_id )) == null ? '' : __t) +
'/' +
((__t = ( user.token )) == null ? '' : __t) +
'"\r\n                                                                    style="display:inline-block;background:#1a7ba6;color:#FFFFFF;font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"\r\n                                                                    target="_blank"\r\n                                                            >\r\n                                                                Cliquez ici pour en choisir un nouveau\r\n                                                            </a>\r\n                                                        </td>\r\n                                                    </tr>\r\n                                                </table>\r\n                                            </td>\r\n                                            </tr>\r\n                                            </tbody>\r\n                                        </table>\r\n                        </div>\r\n                        <!--[if mso | IE]></td>\r\n                    <td class="" style="vertical-align:top;width:60px;"><![endif]-->\r\n                        <div\r\n                                class="mj-column-per-10 mj-outlook-group-fix"\r\n                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                        >\r\n                            <table\r\n                                    border="0"\r\n                                    cellpadding="0"\r\n                                    cellspacing="0"\r\n                                    role="presentation"\r\n                                    style="vertical-align:top;"\r\n                                    width="100%"\r\n                            >\r\n                                <tbody></tbody>\r\n                            </table>\r\n                        </div>\r\n                        <!--[if mso | IE]></td></tr></table><![endif]-->\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n        <!--[if mso | IE]></td></tr></table>\r\n    <table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600">\r\n        <tr>\r\n            <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n        <div style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;max-width:600px;">\r\n            <table\r\n                    align="center"\r\n                    border="0"\r\n                    cellpadding="0"\r\n                    cellspacing="0"\r\n                    role="presentation"\r\n                    style="background:#f2f2f2;background-color:#f2f2f2;width:100%;"\r\n            >\r\n                <tbody>\r\n                <tr>\r\n                    <td\r\n                            style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:30px;text-align:center;"\r\n                    >\r\n                        <!--[if mso | IE]>\r\n                        <table role="presentation" border="0" cellpadding="0" cellspacing="0">\r\n                            <tr>\r\n                                <td class="" style="vertical-align:top;width:170px;"><![endif]-->\r\n                        <div\r\n                                class="mj-column-px-170 mj-outlook-group-fix"\r\n                                style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                        >\r\n                            <table\r\n                                    border="0"\r\n                                    cellpadding="0"\r\n                                    cellspacing="0"\r\n                                    role="presentation"\r\n                                    style="vertical-align:top;"\r\n                                    width="100%"\r\n                            >\r\n                                <tbody>\r\n                                <tr>\r\n                                    <td\r\n                                            align="center"\r\n                                            style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;"\r\n                                    >\r\n                                        <table\r\n                                                border="0"\r\n                                                cellpadding="0"\r\n                                                cellspacing="0"\r\n                                                role="presentation"\r\n                                                style="border-collapse:collapse;border-spacing:0px;"\r\n                                        >\r\n                                            <tbody>\r\n                                            <tr>\r\n                                                <td align="center" bgcolor="#00529F" role="presentation"\r\n                                                    style="border:none;border-radius:30px;cursor:auto;mso-padding-alt:10px 25px;background:#00529F;"\r\n                                                    valign="middle">\r\n                                                    <a href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'/definir-mot-de-passe/' +
((__t = ( user.email )) == null ? '' : __t) +
'/' +
((__t = ( utilisateur_id )) == null ? '' : __t) +
'/' +
((__t = ( user.token )) == null ? '' : __t) +
'"\r\n                                                       style="display:inline-block;background:#00529F;color:#FFFFFF;font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:14px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:30px;"\r\n                                                       target="_blank"> Je définis un nouveau mot de passe </a>\r\n                                                </td>\r\n                                            </tr>\r\n                                            </tbody>\r\n                                        </table>\r\n                                    </td>\r\n                                </tr>\r\n                                </tbody>\r\n                            </table>\r\n                        </div>\r\n                        <!--[if mso | IE]></td></tr></table><![endif]-->\r\n                    </td>\r\n                </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n    <!--[if mso | IE]></v:textbox></v:rect></td></tr></table>\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;"\r\n       width="600" bgcolor="#317FCB">\r\n    <tr>\r\n        <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n    <div style="background:#317FCB;background-color:#317FCB;margin:0px auto;max-width:600px;">\r\n        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"\r\n               style="background:#317FCB;background-color:#317FCB;width:100%;">\r\n            <tbody>\r\n            <tr>\r\n                <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:20px;padding-top:20px;text-align:center;">\r\n                    <!--[if mso | IE]>\r\n                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">\r\n                        <tr>\r\n                            <td class="" style="vertical-align:top;width:600px;"><![endif]-->\r\n                    <div class="mj-column-per-100 mj-outlook-group-fix"\r\n                         style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\r\n                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"\r\n                               style="vertical-align:top;" width="100%">\r\n                            <tbody>\r\n                            <tr>\r\n                                <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">\r\n                                    <div style="font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:12px;line-height:18px;text-align:center;color:#ffffff;">\r\n                                        Copyright ©FFB<br/>Tous droits réservés.\r\n                                    </div>\r\n                                </td>\r\n                            </tr>\r\n                            </tbody>\r\n                        </table>\r\n                    </div>\r\n                    <!--[if mso | IE]></td></tr></table><![endif]-->\r\n                </td>\r\n            </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n    <!--[if mso | IE]></td></tr></table><![endif]-->\r\n</div>\r\n</body>\r\n\r\n</html>\r\n';

}
return __p
}

/***/ }),

/***/ "./app/templates/mails/account-new-password.txt":
/*!******************************************************!*\
  !*** ./app/templates/mails/account-new-password.txt ***!
  \******************************************************/
/***/ ((module) => {

module.exports = function(obj) {
obj || (obj = {});
var __t, __p = '';
with (obj) {
__p += '<!doctype html>\r\n<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">\r\n\r\n<head>\r\n  <title>\r\n  </title>\r\n  <!--[if !mso]><!-->\r\n  <meta http-equiv="X-UA-Compatible" content="IE=edge">\r\n  <!--<![endif]-->\r\n  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">\r\n  <meta name="viewport" content="width=device-width, initial-scale=1">\r\n  <style type="text/css">\r\n    #outlook a {\r\n      padding: 0;\r\n    }\r\n\r\n    body {\r\n      margin: 0;\r\n      padding: 0;\r\n      -webkit-text-size-adjust: 100%;\r\n      -ms-text-size-adjust: 100%;\r\n    }\r\n\r\n    table,\r\n    td {\r\n      border-collapse: collapse;\r\n      mso-table-lspace: 0pt;\r\n      mso-table-rspace: 0pt;\r\n    }\r\n\r\n    img {\r\n      border: 0;\r\n      height: auto;\r\n      line-height: 100%;\r\n      outline: none;\r\n      text-decoration: none;\r\n      -ms-interpolation-mode: bicubic;\r\n    }\r\n\r\n    p {\r\n      display: block;\r\n      margin: 13px 0;\r\n    }\r\n\r\n  </style>\r\n  <!--[if mso]>\r\n    <noscript>\r\n    <xml>\r\n    <o:OfficeDocumentSettings>\r\n      <o:AllowPNG/>\r\n      <o:PixelsPerInch>96</o:PixelsPerInch>\r\n    </o:OfficeDocumentSettings>\r\n    </xml>\r\n    </noscript>\r\n    <![endif]-->\r\n  <!--[if lte mso 11]>\r\n    <style type="text/css">\r\n      .mj-outlook-group-fix { width:100% !important; }\r\n    </style>\r\n    <![endif]-->\r\n  <style type="text/css">\r\n    @media only screen and (min-width:480px) {\r\n      .mj-column-per-50 {\r\n        width: 50% !important;\r\n        max-width: 50%;\r\n      }\r\n\r\n      .mj-column-per-100 {\r\n        width: 100% !important;\r\n        max-width: 100%;\r\n      }\r\n    }\r\n\r\n  </style>\r\n  <style media="screen and (min-width:480px)">\r\n    .moz-text-html .mj-column-per-50 {\r\n      width: 50% !important;\r\n      max-width: 50%;\r\n    }\r\n\r\n    .moz-text-html .mj-column-per-100 {\r\n      width: 100% !important;\r\n      max-width: 100%;\r\n    }\r\n\r\n  </style>\r\n  <style type="text/css">\r\n    @media only screen and (max-width:480px) {\r\n      table.mj-full-width-mobile {\r\n        width: 100% !important;\r\n      }\r\n\r\n      td.mj-full-width-mobile {\r\n        width: auto !important;\r\n      }\r\n    }\r\n\r\n  </style>\r\n  <style type="text/css">\r\n  </style>\r\n</head>\r\n\r\n<body style="word-spacing:normal;background-color:#f2f2f2;">\r\n  <div style="background-color:#f2f2f2;">\r\n    <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n    <div style="margin:0px auto;max-width:600px;">\r\n      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">\r\n        <tbody>\r\n          <tr>\r\n            <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:20px;padding-top:20px;text-align:center;">\r\n              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:middle;width:300px;" ><![endif]-->\r\n              <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">\r\n                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:middle;" width="100%">\r\n                  <tbody>\r\n                    <tr>\r\n                      <td align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:25px;padding-bottom:0px;padding-left:25px;word-break:break-word;">\r\n                        <div style="font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:11px;line-height:1;text-align:left;color:#000000;"><span style="font-size: 15px">Choisissez un nouveau mot de passe</span></div>\r\n                      </td>\r\n                    </tr>\r\n                  </tbody>\r\n                </table>\r\n              </div>\r\n              <!--[if mso | IE]></td></tr></table><![endif]-->\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><v:rect style="width:600px;" xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"><v:fill origin="0.5, 0" position="0.5, 0" src="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg" color="#ffffff" type="tile" size="1,1" aspect="atleast" /><v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0"><![endif]-->\r\n    <div style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg\') center top / cover repeat;background-position:center top;background-repeat:repeat;background-size:cover;margin:0px auto;max-width:600px;">\r\n      <div style="line-height:0;font-size:0;">\r\n        <table align="center" background="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/header-bkg.jpg\') center top / cover repeat;background-position:center top;background-repeat:repeat;background-size:cover;width:100%;">\r\n          <tbody>\r\n            <tr>\r\n              <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:0;text-align:center;">\r\n                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->\r\n                <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\r\n                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\r\n                    <tbody>\r\n                      <tr>\r\n                        <td align="left" style="font-size:0px;padding:10px 25px;padding-top:0;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;">\r\n                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">\r\n                            <tbody>\r\n                              <tr>\r\n                                <td style="width:231px;">\r\n                                  <a href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'" target="_blank">\r\n                                    <img alt="FFB logo" height="auto" src="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/logo.png" style="border:none;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="231" />\r\n                                  </a>\r\n                                </td>\r\n                              </tr>\r\n                            </tbody>\r\n                          </table>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n                <!--[if mso | IE]></td></tr></table><![endif]-->\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n    </div>\r\n    <!--[if mso | IE]></v:textbox></v:rect></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#ffffff" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><v:rect style="width:600px;" xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false"><v:fill origin="0, -0.5" position="0, -0.5" src="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg" color="#ffffff" type="frame" size="100%" aspect="atmost" /><v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0"><![endif]-->\r\n    <div style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg\') center top / 100% no-repeat;background-position:center top;background-repeat:no-repeat;background-size:100%;margin:0px auto;max-width:600px;">\r\n      <div style="line-height:0;font-size:0;">\r\n        <table align="center" background="' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff url(\'' +
((__t = ( APP_URL_STATIC )) == null ? '' : __t) +
'/mails/body-bkg.jpg\') center top / 100% no-repeat;background-position:center top;background-repeat:no-repeat;background-size:100%;width:100%;">\r\n          <tbody>\r\n            <tr>\r\n              <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:40px;padding-top:20px;text-align:center;">\r\n                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->\r\n                <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\r\n                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\r\n                    <tbody>\r\n                      <tr>\r\n                        <td align="justify" style="font-size:0px;padding:10px 25px;word-break:break-word;">\r\n                          <div style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:justify;color:#000000;">Bonjour <strong>' +
((__t = ( user.pseudo )) == null ? '' : __t) +
'</strong>,</div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td align="justify" style="font-size:0px;padding:10px 25px;word-break:break-word;">\r\n                          <div style="font-family:Helvetica, Arial, sans-serif;font-size:13px;line-height:1;text-align:justify;color:#000000;">Vous avez perdu votre mot de passe.</div>\r\n                        </td>\r\n                      </tr>\r\n                      <tr>\r\n                        <td\r\n                          align="center"\r\n                          vertical-align="middle"\r\n                          style="font-size:0px;padding:15px 30px;padding-top:10px;padding-right:25px;padding-bottom:10px;padding-left:25px;word-break:break-word;"\r\n                        >\r\n                          <table\r\n                            border="0"\r\n                            cellpadding="0"\r\n                            cellspacing="0"\r\n                            role="presentation"\r\n                            style="border-collapse:separate;line-height:100%;"\r\n                          >\r\n                            <tr>\r\n                              <td\r\n                                align="center"\r\n                                bgcolor="#1a7ba6"\r\n                                role="presentation"\r\n                                style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#1a7ba6;"\r\n                                valign="middle"\r\n                              >\r\n                                <a\r\n                                  href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'/definir-mot-de-passe/' +
((__t = ( user.email )) == null ? '' : __t) +
'/' +
((__t = ( user.utilisateur_id )) == null ? '' : __t) +
'/' +
((__t = ( user.token )) == null ? '' : __t) +
'"\r\n                                  style="display:inline-block;background:#1a7ba6;color:#FFFFFF;font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"\r\n                                  target="_blank"\r\n                                >\r\n                                  Cliquez ici pour en choisir un nouveau\r\n                                </a>\r\n                              </td>\r\n                            </tr>\r\n                          </table>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n                <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:60px;" ><![endif]-->\r\n                <div\r\n                  class="mj-column-per-10 mj-outlook-group-fix"\r\n                  style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                >\r\n                  <table\r\n                    border="0"\r\n                    cellpadding="0"\r\n                    cellspacing="0"\r\n                    role="presentation"\r\n                    style="vertical-align:top;"\r\n                    width="100%"\r\n                  >\r\n                    <tbody></tbody>\r\n                  </table>\r\n                </div>\r\n                <!--[if mso | IE]></td></tr></table><![endif]-->\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n      <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n      <div style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;max-width:600px;">\r\n        <table\r\n          align="center"\r\n          border="0"\r\n          cellpadding="0"\r\n          cellspacing="0"\r\n          role="presentation"\r\n          style="background:#f2f2f2;background-color:#f2f2f2;width:100%;"\r\n        >\r\n          <tbody>\r\n            <tr>\r\n              <td\r\n                style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:30px;text-align:center;"\r\n              >\r\n                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:170px;" ><![endif]-->\r\n                <div\r\n                  class="mj-column-px-170 mj-outlook-group-fix"\r\n                  style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                >\r\n                  <table\r\n                    border="0"\r\n                    cellpadding="0"\r\n                    cellspacing="0"\r\n                    role="presentation"\r\n                    style="vertical-align:top;"\r\n                    width="100%"\r\n                  >\r\n                    <tbody>\r\n                      <tr>\r\n                        <td\r\n                          align="center"\r\n                          style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;"\r\n                        >\r\n                          <table\r\n                            border="0"\r\n                            cellpadding="0"\r\n                            cellspacing="0"\r\n                            role="presentation"\r\n                            style="border-collapse:collapse;border-spacing:0px;"\r\n                          >\r\n                        <td\r\n                          align="center"\r\n                          vertical-align="middle"\r\n                          style="font-size:0px;padding:15px 30px;padding-top:10px;padding-right:25px;padding-bottom:10px;padding-left:25px;word-break:break-word;"\r\n                        >\r\n                          <table\r\n                            border="0"\r\n                            cellpadding="0"\r\n                            cellspacing="0"\r\n                            role="presentation"\r\n                            style="border-collapse:separate;line-height:100%;"\r\n                          >\r\n                            <tr>\r\n                              <td\r\n                                align="center"\r\n                                bgcolor="#1a7ba6"\r\n                                role="presentation"\r\n                                style="border:none;border-radius:3px;cursor:auto;mso-padding-alt:10px 25px;background:#1a7ba6;"\r\n                                valign="middle"\r\n                              >\r\n                                <a\r\n                                  href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'/definir-mot-de-passe/' +
((__t = ( user.email )) == null ? '' : __t) +
'/' +
((__t = ( user.utilisateur_id )) == null ? '' : __t) +
'/' +
((__t = ( user.token )) == null ? '' : __t) +
'"\r\n                                  style="display:inline-block;background:#1a7ba6;color:#FFFFFF;font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:18px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:3px;"\r\n                                  target="_blank"\r\n                                >\r\n                                  Cliquez ici pour en choisir un nouveau\r\n                                </a>\r\n                              </td>\r\n                            </tr>\r\n                          </table>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n                <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:60px;" ><![endif]-->\r\n                <div\r\n                  class="mj-column-per-10 mj-outlook-group-fix"\r\n                  style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                >\r\n                  <table\r\n                    border="0"\r\n                    cellpadding="0"\r\n                    cellspacing="0"\r\n                    role="presentation"\r\n                    style="vertical-align:top;"\r\n                    width="100%"\r\n                  >\r\n                    <tbody></tbody>\r\n                  </table>\r\n                </div>\r\n                <!--[if mso | IE]></td></tr></table><![endif]-->\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n      <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n      <div style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;max-width:600px;">\r\n        <table\r\n          align="center"\r\n          border="0"\r\n          cellpadding="0"\r\n          cellspacing="0"\r\n          role="presentation"\r\n          style="background:#f2f2f2;background-color:#f2f2f2;width:100%;"\r\n        >\r\n          <tbody>\r\n            <tr>\r\n              <td\r\n                style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:0px;padding-top:30px;text-align:center;"\r\n              >\r\n                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:170px;" ><![endif]-->\r\n                <div\r\n                  class="mj-column-px-170 mj-outlook-group-fix"\r\n                  style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"\r\n                >\r\n                  <table\r\n                    border="0"\r\n                    cellpadding="0"\r\n                    cellspacing="0"\r\n                    role="presentation"\r\n                    style="vertical-align:top;"\r\n                    width="100%"\r\n                  >\r\n                    <tbody>\r\n                      <tr>\r\n                        <td\r\n                          align="center"\r\n                          style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;word-break:break-word;"\r\n                        >\r\n                          <table\r\n                            border="0"\r\n                            cellpadding="0"\r\n                            cellspacing="0"\r\n                            role="presentation"\r\n                            style="border-collapse:collapse;border-spacing:0px;"\r\n                          >\r\n                            <tbody>\r\n                              <tr>\r\n                                <td align="center" bgcolor="#00529F" role="presentation" style="border:none;border-radius:30px;cursor:auto;mso-padding-alt:10px 25px;background:#00529F;" valign="middle">\r\n                                  <a href="' +
((__t = ( APP_URL_FRONT )) == null ? '' : __t) +
'/definir-mot-de-passe/' +
((__t = ( user.email )) == null ? '' : __t) +
'/' +
((__t = ( utilisateur_id )) == null ? '' : __t) +
'/' +
((__t = ( user.token )) == null ? '' : __t) +
'" style="display:inline-block;background:#00529F;color:#FFFFFF;font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:14px;font-weight:normal;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:10px 25px;mso-padding-alt:0px;border-radius:30px;" target="_blank"> Je définis un nouveau mot de passe </a>\r\n                                </td>\r\n                              </tr>\r\n                            </tbody>\r\n                          </table>\r\n                        </td>\r\n                      </tr>\r\n                    </tbody>\r\n                  </table>\r\n                </div>\r\n                <!--[if mso | IE]></td></tr></table><![endif]-->\r\n              </td>\r\n            </tr>\r\n          </tbody>\r\n        </table>\r\n      </div>\r\n    </div>\r\n    <!--[if mso | IE]></v:textbox></v:rect></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" role="presentation" style="width:600px;" width="600" bgcolor="#317FCB" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->\r\n    <div style="background:#317FCB;background-color:#317FCB;margin:0px auto;max-width:600px;">\r\n      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#317FCB;background-color:#317FCB;width:100%;">\r\n        <tbody>\r\n          <tr>\r\n            <td style="direction:ltr;font-size:0px;padding:20px 0;padding-bottom:20px;padding-top:20px;text-align:center;">\r\n              <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:600px;" ><![endif]-->\r\n              <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">\r\n                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">\r\n                  <tbody>\r\n                    <tr>\r\n                      <td align="center" style="font-size:0px;padding:10px 25px;word-break:break-word;">\r\n                        <div style="font-family:Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:12px;line-height:18px;text-align:center;color:#ffffff;">Copyright ©FFB<br />Tous droits réservés.</div>\r\n                      </td>\r\n                    </tr>\r\n                  </tbody>\r\n                </table>\r\n              </div>\r\n              <!--[if mso | IE]></td></tr></table><![endif]-->\r\n            </td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n    </div>\r\n    <!--[if mso | IE]></td></tr></table><![endif]-->\r\n  </div>\r\n</body>\r\n\r\n</html>\r\n';

}
return __p
}

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("body-parser");

/***/ }),

/***/ "compression":
/*!******************************!*\
  !*** external "compression" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("compression");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("cors");

/***/ }),

/***/ "dotenv":
/*!*************************!*\
  !*** external "dotenv" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = require("dotenv");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("express");

/***/ }),

/***/ "express-fileupload":
/*!*************************************!*\
  !*** external "express-fileupload" ***!
  \*************************************/
/***/ ((module) => {

"use strict";
module.exports = require("express-fileupload");

/***/ }),

/***/ "joi":
/*!**********************!*\
  !*** external "joi" ***!
  \**********************/
/***/ ((module) => {

"use strict";
module.exports = require("joi");

/***/ }),

/***/ "js-sha512":
/*!****************************!*\
  !*** external "js-sha512" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("js-sha512");

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = require("lodash");

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = require("moment");

/***/ }),

/***/ "mongodb":
/*!**************************!*\
  !*** external "mongodb" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("mongodb");

/***/ }),

/***/ "morgan":
/*!*************************!*\
  !*** external "morgan" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = require("morgan");

/***/ }),

/***/ "node-mailjet":
/*!*******************************!*\
  !*** external "node-mailjet" ***!
  \*******************************/
/***/ ((module) => {

"use strict";
module.exports = require("node-mailjet");

/***/ }),

/***/ "nodemailer":
/*!*****************************!*\
  !*** external "nodemailer" ***!
  \*****************************/
/***/ ((module) => {

"use strict";
module.exports = require("nodemailer");

/***/ }),

/***/ "request-ip":
/*!*****************************!*\
  !*** external "request-ip" ***!
  \*****************************/
/***/ ((module) => {

"use strict";
module.exports = require("request-ip");

/***/ }),

/***/ "slug":
/*!***********************!*\
  !*** external "slug" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("slug");

/***/ }),

/***/ "sparkpost":
/*!****************************!*\
  !*** external "sparkpost" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("sparkpost");

/***/ }),

/***/ "cluster":
/*!**************************!*\
  !*** external "cluster" ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = require("cluster");

/***/ }),

/***/ "crypto":
/*!*************************!*\
  !*** external "crypto" ***!
  \*************************/
/***/ ((module) => {

"use strict";
module.exports = require("crypto");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ ((module) => {

"use strict";
module.exports = require("fs");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("http");

/***/ }),

/***/ "os":
/*!*********************!*\
  !*** external "os" ***!
  \*********************/
/***/ ((module) => {

"use strict";
module.exports = require("os");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ ((module) => {

"use strict";
module.exports = require("path");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!********************!*\
  !*** ./app/App.js ***!
  \********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var cluster__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cluster */ "cluster");
/* harmony import */ var cluster__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(cluster__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! http */ "http");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var compression__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! compression */ "compression");
/* harmony import */ var compression__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(compression__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var morgan__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! morgan */ "morgan");
/* harmony import */ var morgan__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(morgan__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! body-parser */ "body-parser");
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var express_fileupload__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! express-fileupload */ "express-fileupload");
/* harmony import */ var express_fileupload__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(express_fileupload__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var os__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! os */ "os");
/* harmony import */ var os__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(os__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! path */ "path");
/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _configs_ServerConfig__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./configs/ServerConfig */ "./app/configs/ServerConfig.js");
/* harmony import */ var _utils_loggerFormat__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./utils/loggerFormat */ "./app/utils/loggerFormat.js");
/* harmony import */ var _utils_RequestsUtils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./utils/RequestsUtils */ "./app/utils/RequestsUtils.js");
/* harmony import */ var _routes_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./routes/index */ "./app/routes/index.js");
/*****************************************************************************
 * API Server
 *****************************************************************************/

/*
 * Import Vendors
 */








 // import socketIO from 'socket.io';
//import expressStatic from 'express-static';

/*
 * Import Configs
 */




/*
 * Import Router
 */



if ((cluster__WEBPACK_IMPORTED_MODULE_0___default().isPrimary)) {
  // Master cluster (start an API for each CPU core)
  console.log("LesJVnodeJs Server v".concat("1.0.0")); // Create a worker for each CPU

  var cpuCount = process.env.APP_CLUSTER || os__WEBPACK_IMPORTED_MODULE_7___default().cpus().length;

  for (var i = 0; i < cpuCount; i += 1) {
    cluster__WEBPACK_IMPORTED_MODULE_0___default().fork();
  } // Recreate worker when dead


  cluster__WEBPACK_IMPORTED_MODULE_0___default().on('exit', function (worker) {
    console.error("API Worker ".concat(worker.id, " died :("));
    cluster__WEBPACK_IMPORTED_MODULE_0___default().fork();
  });
} else {
  // API cluster (a CPU core)

  /*
   * App creation
   */
  var app = express__WEBPACK_IMPORTED_MODULE_1___default()();

  var requestIp = __webpack_require__(/*! request-ip */ "request-ip");

  var localpath =  false ? 0 : '../../';
  var http = (0,http__WEBPACK_IMPORTED_MODULE_2__.Server)(app); // app.ioSocket = socketIO(http);
  // get IP

  app.use(requestIp.mw());
  app.use(compression__WEBPACK_IMPORTED_MODULE_3___default()());
  app.use(body_parser__WEBPACK_IMPORTED_MODULE_5___default().json({
    limit: '150mb'
  }));
  app.use(body_parser__WEBPACK_IMPORTED_MODULE_5___default().urlencoded({
    extended: true
  }));
  app.use(express_fileupload__WEBPACK_IMPORTED_MODULE_6___default()());
  app.use(morgan__WEBPACK_IMPORTED_MODULE_4___default()(_utils_loggerFormat__WEBPACK_IMPORTED_MODULE_10__["default"]));
  app.use(express__WEBPACK_IMPORTED_MODULE_1___default()["static"](path__WEBPACK_IMPORTED_MODULE_8___default().join(__dirname, localpath + 'lesjvdanaellenodejs/static')));
  app.use('/static', express__WEBPACK_IMPORTED_MODULE_1___default()["static"](path__WEBPACK_IMPORTED_MODULE_8___default().join(__dirname, '../app/static')));

  if (true) {
    app.use(__webpack_require__(/*! cors */ "cors")());
  } //---------------------------NOTIF-----------------------------------------
  // NOTIF public/private keys


  var mailVapidKey = process.env.MAIL_VAPID_KEY;
  var publicVapidKey = process.env.PUBLIC_VAPID_KEY;
  var privateVapidKey = process.env.PRIVATE_VAPID_KEY; //webpush.setVapidDetails(`mailto:${mailVapidKey}`, publicVapidKey, privateVapidKey);

  /*
   * API routes
   */

  app.use(_utils_RequestsUtils__WEBPACK_IMPORTED_MODULE_11__.checkHeadersAcceptJSONResponse);
  app.use('/api', _routes_index__WEBPACK_IMPORTED_MODULE_12__["default"]);
  /*
   * Go, go, go :D
   */

  http.listen(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_9__.SERVER.APP_PORT, function () {
    if ((cluster__WEBPACK_IMPORTED_MODULE_0___default().isWorker)) {
      console.log("API worker # ".concat((cluster__WEBPACK_IMPORTED_MODULE_0___default().worker.id), " listening on port ").concat(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_9__.SERVER.APP_PORT, " in ").concat("development", " environment."));
    }

    console.log("API single worker listening on port ".concat(_configs_ServerConfig__WEBPACK_IMPORTED_MODULE_9__.SERVER.APP_PORT, " in ").concat("development", " environment."));
  });
}
})();

/******/ })()
;
//# sourceMappingURL=App.js.map